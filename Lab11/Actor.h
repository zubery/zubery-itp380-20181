#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"

class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	
	Actor(class Game* game);
	virtual ~Actor();

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);
	// ProcessInput function called from Game (not overridable)
	void ProcessInput(const Uint8* keyState);
	// Any actor-specific update code (overridable)
	virtual void ActorInput(const Uint8* keyState);

	// Getters/setters
	const Vector3& GetPosition() const { return mPosition; }
	void SetPosition(const Vector3& pos) { mPosition = pos; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale; }
	float GetRotation() const { return mRotation; }
	void SetRotation(float rotation) { mRotation = rotation; }
	
	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }

	class Game* GetGame() { return mGame; }

    Vector3 GetForward() const;
    Vector3 GetRight() const;
    
    class MoveComponent* GetMoveComponent() { return mComp; }
    class CollisionComponent* GetCollisionComponent() { return mColl; }
    const Matrix4& GetWorldTransform() const { return mWorldTransform; }
    class MeshComponent* GetMesh() { return mMesh; }
    class CameraComponent* GetCamera() { return mCam; }
    
    void SetRespawnPos(Vector3 pos) { respawnPos = pos; }
    Vector3 GetRespawnPos() { return respawnPos; }
    
    void SetQuat(Quaternion newQuat) { mQuat = newQuat; }
    Quaternion GetQuat() { return mQuat; }
    
protected:
	class Game* mGame;
	// Actor's state
	State mState;
	// Transform
	Vector3 mPosition;
	float mScale;
	float mRotation;
    class MoveComponent* mComp;
    class CollisionComponent* mColl;
    Matrix4 mWorldTransform;
    class MeshComponent* mMesh;
    class CameraComponent* mCam;
    Vector3 respawnPos;
    Quaternion mQuat;
};
