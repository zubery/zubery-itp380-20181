#include "CollisionComponent.h"
#include "Actor.h"

CollisionComponent::CollisionComponent(class Actor* owner)
:Component(owner)
,mWidth(0.0f)
,mHeight(0.0f)
{
	
}

CollisionComponent::~CollisionComponent()
{
	
}

bool CollisionComponent::Intersect(const CollisionComponent* other)
{
	// TODO: Implement
    if ((this -> GetMin()).x <= (other -> GetMax()).x &&
        (this -> GetMax()).x >= (other -> GetMin()).x &&
        (this -> GetMin()).y <= (other -> GetMax()).y &&
        (this -> GetMax()).y >= (other -> GetMin()).y)
    {
        return true;
    }
    else {
        return false;
    }
}

Vector2 CollisionComponent::GetMin() const
{
    Vector2 min;
    min.x = (mOwner -> GetPosition()).x - ((mWidth * (mOwner -> GetScale())) / 2.0f);
    min.y = (mOwner -> GetPosition()).y - ((mHeight * (mOwner -> GetScale())) / 2.0f);
	return min;
}

Vector2 CollisionComponent::GetMax() const
{
	// TODO: Implement
    Vector2 max;
    max.x = (mOwner -> GetPosition()).x + ((mWidth * (mOwner -> GetScale())) / 2.0f);
    max.y = (mOwner -> GetPosition()).y + ((mHeight * (mOwner -> GetScale())) / 2.0f);
	return max;
}

const Vector2& CollisionComponent::GetCenter() const
{
	return mOwner->GetPosition();
}

