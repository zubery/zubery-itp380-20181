//
//  Bullet.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Bullet.hpp"
#include "MoveComponent.h"
#include "Game.h"
#include "MeshComponent.h"
#include "Renderer.h"
#include "CollisionComponent.h"
#include "Block.hpp"
#include "Tank.hpp"

Bullet::Bullet(Game* mGame) : Actor(mGame) {
    mComp = new MoveComponent(this);
    mMesh = new MeshComponent(this);
    mMesh -> SetMesh(mGame -> GetRenderer() -> GetMesh("Assets/Sphere.gpmesh"));
    mColl = new CollisionComponent(this);
    mColl -> SetSize(10.0f, 10.0f, 10.0f);
    this -> SetScale(0.5f);
    mComp -> SetForwardSpeed(400.0f);
}

Bullet::~Bullet() {
    
}

void Bullet::UpdateActor(float deltaTime) {
    for(int i = 0; i < mGame -> GetNumBlocks(); i++) {
        if(this -> GetCollisionComponent() -> Intersect((mGame -> GetBlocks())[i] -> GetCollisionComponent())) {
            this -> SetState(Actor::EDead);
            break;
        }
    }
    if(this -> GetCollisionComponent() -> Intersect(mGame -> GetTank1() -> GetCollisionComponent()) && mGame -> GetTank1() != this -> GetShot()) {
        mGame -> GetTank1() -> Respawn();
        this -> SetState(Actor::EDead);
    }
    if(this -> GetCollisionComponent() -> Intersect(mGame -> GetTank2() -> GetCollisionComponent()) && mGame -> GetTank2() != this -> GetShot()) {
        mGame -> GetTank2() -> Respawn();
        this -> SetState(Actor::EDead);
    }
}
