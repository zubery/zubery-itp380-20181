//
//  Paddle.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/1/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Paddle_hpp
#define Paddle_hpp

#include <stdio.h>
#include "Actor.h"

class Paddle : public Actor {
public:
    Paddle(class Game* mGame);
    ~Paddle();
    
    void ActorInput(const Uint8 *keyState) override;
};

#endif /* Paddle_hpp */
