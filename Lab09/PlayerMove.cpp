//
//  PlayerMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PlayerMove.hpp"
#include "Game.h"
#include "Actor.h"
#include "CameraComponent.hpp"
#include "CollisionComponent.h"
#include "Block.hpp"

PlayerMove::PlayerMove(Actor* owner) : MoveComponent(owner) {
    this -> ChangeState(MoveState::Falling);
    mZSpeed = 0.0f;
}

PlayerMove::~PlayerMove() {
    
}

void PlayerMove::Update(float deltatime) {
    if(mCurrentState == MoveState::OnGround) {
        this -> UpdateOnGround(deltatime);
    }
    else if(mCurrentState == MoveState::Falling) {
        this -> UpdateFalling(deltatime);
    }
    else if(mCurrentState == MoveState::Jump) {
        this -> UpdateJump(deltatime);
    }
}

void PlayerMove::ProcessInput(const Uint8 *keyState) {
    int x, y;
    SDL_GetRelativeMouseState(&x, &y);
    
    float xAngular;
    xAngular = x / 500.0f;
    xAngular *= (M_PI * 10.0f);
    this -> SetAngularSpeed(xAngular);
    
    float yAngular;
    yAngular = y / 500.0f;
    yAngular *= (M_PI * 10.0f);
    this -> mOwner -> GetCamera() -> SetPitchSpeed(yAngular);
    
    if(keyState[SDL_SCANCODE_W] && keyState[SDL_SCANCODE_S]) {
        this -> SetForwardSpeed(0.0f);
    }
    else if(keyState[SDL_SCANCODE_W]) {
        this -> SetForwardSpeed(350.0f);
    }
    else if(keyState[SDL_SCANCODE_S]) {
        this -> SetForwardSpeed(-350.0f);
    }
    else {
        this -> SetForwardSpeed(0.0f);
    }
    
    if(keyState[SDL_SCANCODE_A] && keyState[SDL_SCANCODE_D]) {
        this -> SetStrafeSpeed(0.0f);
    }
    else if(keyState[SDL_SCANCODE_A]) {
        this -> SetStrafeSpeed(-350.0f);
    }
    else if(keyState[SDL_SCANCODE_D]) {
        this -> SetStrafeSpeed(350.0f);
    }
    else {
        this -> SetStrafeSpeed(0.0f);
    }
    
    if(!spacePressed && keyState[SDL_SCANCODE_SPACE]) {
        if(mCurrentState == MoveState::OnGround) {
            mZSpeed = JumpSpeed;
            this -> ChangeState(PlayerMove::MoveState::Jump);
            spacePressed = true;
        }
    }
    else if (!keyState[SDL_SCANCODE_SPACE]) {
        spacePressed = false;
    }
}

void PlayerMove::UpdateJump(float deltatime) {
    MoveComponent::Update(deltatime);
    mZSpeed += Gravity * deltatime;
    mOwner -> SetPosition(mOwner -> GetPosition() + Vector3(0, 0, mZSpeed * deltatime));
    
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        if(check == PlayerMove::CollSide::Bottom) {
            mZSpeed = 0.0f;
        }
    }
    if(mZSpeed <= 0.0f) {
        this -> ChangeState(PlayerMove::MoveState::Falling);
    }
    
}

void PlayerMove::UpdateFalling(float deltatime) {
    MoveComponent::Update(deltatime);
    mZSpeed += Gravity * deltatime;
    mOwner -> SetPosition(mOwner -> GetPosition() + Vector3(0, 0, mZSpeed * deltatime));
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        if(check == PlayerMove::CollSide::Top) {
            mZSpeed = 0.0f;
            this -> ChangeState(PlayerMove::MoveState::OnGround);
        }
    }
}

void PlayerMove::UpdateOnGround(float deltatime) {
    bool fall = true;
    MoveComponent::Update(deltatime);
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        if(check == PlayerMove::CollSide::Top) {
            fall = false;
        }
    }
    if(fall) {
        this -> ChangeState(PlayerMove::MoveState::Falling);
    }
}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent *self, CollisionComponent *block) {
    PlayerMove::CollSide collisionSide = PlayerMove::CollSide::None;
    
    float newX = mOwner -> GetPosition().x;
    float newY = mOwner -> GetPosition().y;
    float newZ = mOwner -> GetPosition().z;
    
    mOwner -> SetPosition(Vector3(newX, newY, newZ));
    
    float playerYMin = (self -> GetMin().y);
    float playerYMax = (self -> GetMax().y);
    float playerXMin = (self -> GetMin().x);
    float playerXMax = (self -> GetMax().x);
    float playerZMin = (self -> GetMin().z);
    float playerZMax = (self -> GetMax().z);
    
    if (self -> Intersect(block)) {
        float dx1, dx2, dy1, dy2, dz1, dz2;
        float tempx1, tempx2, tempy1, tempy2, tempz1, tempz2;
        dx2 = (block-> GetMax().x) - playerXMin;
        dx1 = (block -> GetMin().x) - playerXMax;
        dy1 = (block -> GetMin().y) - playerYMax;
        dy2 = (block -> GetMax().y) - playerYMin;
        dz1 = (block -> GetMin().z) - playerZMax;
        dz2 = (block -> GetMax().z) - playerZMin;
            
        tempx2 = dx2;
        tempx1 = dx1;
        tempy2 = dy2;
        tempy1 = dy1;
        tempz1 = dz1;
        tempz2 = dz2;
            
        if(dx2 < 0) {
            tempx2 = dx2 * -1;
        }
        if(dx1 < 0) {
            tempx1 = dx1 * -1;
        }
        if(dy2 < 0) {
            tempy2 = dy2 * -1;
        }
        if(dy1 < 0) {
            tempy1 = dy1 * -1;
        }
        if(dz1 < 0) {
            tempz1 = dz1 * -1;
        }
        if(dz2 < 0) {
            tempz2 = dz2 * -1;
        }
            
        float smallest = std::min(tempz1, std::min(tempz2, std::min(tempx1, std::min(tempx2, std::min(tempy1, tempy2)))));
            
        if(smallest == tempy1) {
            newY += dy1;
            playerYMin += dy1;
            playerYMax += dy1;
            collisionSide = PlayerMove::CollSide::Side;
        }
        else if(smallest == tempy2) {
            newY += dy2;
            playerYMin += dy2;
            playerYMax += dy2;
            collisionSide = PlayerMove::CollSide::Side;
        }
        else if(smallest == tempx1) {
            newX += dx1;
            playerXMin += dx1;
            playerXMax += dx1;
            collisionSide = PlayerMove::CollSide::Side;
        }
        else if(smallest == tempx2) {
            newX += dx2;
            playerXMin += dx2;
            playerXMax += dx2;
            collisionSide = PlayerMove::CollSide::Side;
        }
        else if(smallest == tempz1) {
            newZ += dz1;
            playerZMin += dz1;
            playerZMax += dz1;
            collisionSide = PlayerMove::CollSide::Bottom;
        }
        else if(smallest == tempz2) {
            newZ += dz2;
            playerZMin += dz2;
            playerZMax += dz2;
            collisionSide = PlayerMove::CollSide::Top;
        }
    }
    
    mOwner -> SetPosition(Vector3(newX, newY, newZ));
    return collisionSide;
}
