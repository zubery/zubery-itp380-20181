#include "Grid.h"
#include "Tile.h"
#include <SDL/SDL.h>
#include <algorithm>
#include "Tower.hpp"
#include <iostream>
#include "Plane.hpp"

Grid::Grid(class Game* game)
	:Actor(game)
	,mSelectedTile(nullptr)
{
	// 7 rows, 16 columns
	mTiles.resize(NumRows);
	for (size_t i = 0; i < mTiles.size(); i++)
	{
		mTiles[i].resize(NumCols);
	}
	
	// Create tiles
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			mTiles[i][j] = new Tile(GetGame());
			mTiles[i][j]->SetPosition(Vector2(TileSize/2.0f + j * TileSize, StartY + i * TileSize));
		}
	}
	
	// Set start/end tiles
	GetStartTile()->SetTileState(Tile::EStart);
	GetEndTile()->SetTileState(Tile::EBase);
	
	// Set up adjacency lists
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			if (i > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i-1][j]);
			}
			if (i < NumRows - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i+1][j]);
			}
			if (j > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j-1]);
			}
			if (j < NumCols - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j+1]);
			}
		}
	}
    
    if(this -> TryFindPath()) {
        this -> UpdatePathTiles();
    }
}

void Grid::SelectTile(size_t row, size_t col)
{
	// Make sure it's a valid selection
	Tile::TileState tstate = mTiles[row][col]->GetTileState();
	if (tstate != Tile::EStart && tstate != Tile::EBase)
	{
		// Deselect previous one
		if (mSelectedTile)
		{
			mSelectedTile->ToggleSelect();
		}
		mSelectedTile = mTiles[row][col];
		mSelectedTile->ToggleSelect();
	}
}

Tile* Grid::GetStartTile()
{
	return mTiles[3][0];
}

Tile* Grid::GetEndTile()
{
	return mTiles[3][15];
}

void Grid::ActorInput(const Uint8 * keyState)
{
	// Process mouse click to select a tile
	int x, y;
	Uint32 buttons = SDL_GetMouseState(&x, &y);
	if (SDL_BUTTON(buttons) & SDL_BUTTON_LEFT)
	{
		// Calculate the x/y indices into the grid
		y -= static_cast<int>(StartY - TileSize / 2);
		if (y >= 0)
		{
			x /= static_cast<int>(TileSize);
			y /= static_cast<int>(TileSize);
			if (x >= 0 && x < static_cast<int>(NumCols) && y >= 0 && y < static_cast<int>(NumRows))
			{
				SelectTile(y, x);
			}
		}
	}
    
    if(!spacePressed && keyState[SDL_SCANCODE_SPACE] && mSelectedTile != nullptr) {
        spacePressed = true;
        mNeedToBuild = true;
    }
    else {
        spacePressed = false;
    }
}

void Grid::UpdateActor(float deltaTime)
{
    if(mNeedToBuild) {
        this -> BuildTower(mSelectedTile);
        mNeedToBuild = false;
    }
    timer -= deltaTime;
    
    if(timer <= 0) {
        Plane* spawnPlane = new Plane(mGame);
        timer = 3.0f;
    }
}

bool Grid::TryFindPath() {
    openSet.clear();
    for(int i = 0; i < mTiles.size(); i++) {
        for(int j = 0; j < mTiles[i].size(); j++) {
            mTiles[i][j] -> g = 0.0f;
            mTiles[i][j] -> mInClosedSet = false;
        }
    }

    Tile* currentTile = this -> GetEndTile();
    currentTile -> mInClosedSet = true;
    
    Vector2 endpoint = this -> GetStartTile() -> GetPosition();
    
    while(currentTile != this -> GetStartTile()) {
        for(int i = 0; i < (currentTile -> mAdjacent).size(); i++) {
            Tile* n = (currentTile -> mAdjacent)[i];
            
            if(n -> mInClosedSet) {
                continue;
            }
            else if(std::find(openSet.begin(), openSet.end(), n) != openSet.end()) {
                float new_g = ((currentTile  -> g) + TileSize);
                if (new_g < n -> g) {
                    n -> mParent = currentTile;
                    n -> g = new_g;
                    n -> f = (n -> g) + (n -> h);
                }
            }
            else {
                n -> mParent = currentTile;
                n -> h = abs((n -> GetPosition()).x - endpoint.x) + abs((n -> GetPosition()).y - endpoint.y);
                n -> g = ((n -> mParent -> g) + TileSize);
                n -> f = (n -> g) + (n -> h);
                
                if(!(n -> mBlocked)) {
                    openSet.push_back(n);
                }
            }
        }
        
        if(openSet.empty()) {
            break; 
        }
        
        currentTile = openSet[0];
        for(int i = 1; i < openSet.size(); i++) {
            if((currentTile -> f) > (openSet[i] -> f)) {
                currentTile = openSet[i];
            }
        }

        openSet.erase(std::remove(openSet.begin(), openSet.end(), currentTile), openSet.end());
        currentTile -> mInClosedSet = true;
    }
    
    if(currentTile == this -> GetStartTile()) {
        return true;
    }
    else {
        return false;
    }
}

void Grid::UpdatePathTiles() {
    for(int i = 0; i < mTiles.size(); i++) {
        for(int j = 0; j < mTiles[i].size(); j++) {
            if(mTiles[i][j] != this -> GetStartTile() && mTiles[i][j] != this -> GetEndTile()) {
                mTiles[i][j] -> SetTileState(Tile::EDefault);
            }
        }
    }
    Tile* current = this -> GetStartTile();
    
    while(current -> mParent != nullptr) {
        if(current != this -> GetStartTile() && current != this -> GetEndTile()) {
            current -> SetTileState(Tile::EPath);
        }
        current = current -> mParent;
    }
}

void Grid::BuildTower(Tile* tower) {
    if (!(tower -> mBlocked)) {
        tower -> mBlocked = true;
        if (this -> TryFindPath()) {
            this -> UpdatePathTiles();
            Tower* nTower = new Tower(this -> mGame);
            nTower -> SetPosition(tower -> GetPosition());
        }
        else {
            tower -> mBlocked = false;
            this -> TryFindPath();
        }
    }
}
