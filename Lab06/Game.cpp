//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "Actor.h"
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#include <algorithm>
#include "Component.h"
#include "SpriteComponent.h"
#include <fstream>
#include "Block.hpp"
#include "Player.hpp"
#include "Barrel.hpp"
#include "BarrelSpawner.hpp"
#include <iostream>
#include "Background.hpp"
#include "Coin.hpp"
#include <string>

// TODO

Game::Game() {
  window = nullptr;
  renderer = nullptr;
  runner = true;
    this -> SetCameraPos(Vector2(0, 0));
}

bool Game::Initialize() {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}
    
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);


  window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, SDL_WINDOW_OPENGL);
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
  int flags = IMG_INIT_PNG;
  int initted=IMG_Init(flags);
  if((initted & flags) != flags) {
    printf("IMG_Init: Failed to init required jpg and png support!\n");
    printf("IMG_Init: %s\n", IMG_GetError());
  }

    
    
  top.x = 0;
  top.y = 0;
  top.w = 1024;
  top.h = 20;
  right.x = 1024 - 20;
  right.y = 20;
  right.w = 20;
  right.h = 768 - 2*(20);
  bottom.x = 0;
  bottom.y = 768 - 20;
  bottom.w = 1024;
  bottom.h = 20;

  lastTicks = SDL_GetTicks();

  LoadData();
  return true;
}

void Game::RunLoop() {
  while (runner) {
    ProcessInput();
    UpdateGame();
    GenerateOutput();
  }
}

void Game::ProcessInput() {
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      runner = false;
    }
  }
  const Uint8 *state = SDL_GetKeyboardState(nullptr);
  for (auto i : actors) {
    i -> ProcessInput(state);
  }
    
  if (state[SDL_SCANCODE_ESCAPE]) {
    runner = false;
  }
}

void Game::UpdateGame() {
  while (SDL_GetTicks() - lastTicks < 16) {

  }

  deltaTime = SDL_GetTicks() - lastTicks;
  if (deltaTime > 50) {
    deltaTime = 50;
  }
  deltaTime = deltaTime / 1000.0f;
    
  std::vector<Actor*> actorClones = actors;
  for (auto i : actorClones) {
    i -> Update(deltaTime);
  }
  std::vector<Actor*> deadActors;
  for (auto i : actors) {
      //2 is the state EDead
      if (i -> GetState() == 2) {
          deadActors.push_back(i);
      }
  }
  for (auto i : deadActors) {
    delete i;
  }
}

void Game::GenerateOutput() {
  SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);
  SDL_RenderClear(renderer);

  //SDL_RenderCopy(renderer, GetTexture("Assets/Stars.png"), nullptr, nullptr);
    for(auto it = sprites.begin(); it != sprites.end(); it++) {
        (*it) -> Draw(renderer);
    }
    
  SDL_RenderPresent(renderer);
}

void Game::Shutdown() {
  UnloadData();
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  IMG_Quit();
    Mix_CloseAudio();
}

void Game::AddActor(class Actor* newActor) {
    actors.push_back(newActor);
}

void Game::RemoveActor(class Actor* remActor) {
    auto it = std::find(actors.begin(), actors.end(), remActor);
    actors.erase(it);
}

void Game::LoadData() {
    LoadTexture("Assets/Background.png");
    LoadTexture("Assets/BlockA.png");
    LoadTexture("Assets/BlockB.png");
    LoadTexture("Assets/BlockC.png");
    LoadTexture("Assets/BlockD.png");
    LoadTexture("Assets/BlockE.png");
    LoadTexture("Assets/BlockF.png");
    LoadTexture("Assets/Player/Idle.png");
    LoadTexture("Assets/Barrel.png");
    LoadTexture("Assets/Background/Sky_0.png");
    LoadTexture("Assets/Background/Sky_1.png");
    LoadTexture("Assets/Background/Sky_2.png");
    LoadTexture("Assets/Background/Mid_0.png");
    LoadTexture("Assets/Background/Mid_1.png");
    LoadTexture("Assets/Background/Mid_2.png");
    LoadTexture("Assets/Background/Fore_0.png");
    LoadTexture("Assets/Background/Fore_1.png");
    LoadTexture("Assets/Background/Fore_2.png");
    
    for (int i = 0; i < 16; i++) {
        LoadTexture("Assets/Coin/coin" + std::to_string(i + 1) + ".png");
    }
    
    for (int i = 0; i < 10; i++) {
        LoadTexture("Assets/Player/Run" + std::to_string(i + 1) + ".png");
    }
    
    LoadSound("Assets/Player/Jump.wav");
    LoadSound("Assets/Coin/coin.wav");
    
    Actor* backgroundFore = new Actor(this);
    backgroundFore -> SetPosition(Vector2(-512.0f, 0));
    Background* BGF = new Background(backgroundFore, 100);
    BGF -> AddImage(this -> GetTexture("Assets/Background/Fore_0.png"));
    BGF -> AddImage(this -> GetTexture("Assets/Background/Fore_1.png"));
    BGF -> AddImage(this -> GetTexture("Assets/Background/Fore_2.png"));
    BGF -> setParallax(0.75f);
    backgroundFore -> SetSprite(BGF);
    
    Actor* backgroundMid = new Actor(this);
    backgroundMid -> SetPosition(Vector2(-512.0f, 0));
    Background* BGM = new Background(backgroundMid, 75);
    BGM -> AddImage(this -> GetTexture("Assets/Background/Mid_0.png"));
    BGM -> AddImage(this -> GetTexture("Assets/Background/Mid_1.png"));
    BGM -> AddImage(this -> GetTexture("Assets/Background/Mid_2.png"));
    BGM -> setParallax(0.5f);
    backgroundMid -> SetSprite(BGM);
    
    Actor* backgroundSky = new Actor(this);
    backgroundSky -> SetPosition(Vector2(-512.0f, 0));
    Background* BGS = new Background(backgroundSky, 50);
    BGS -> AddImage(this -> GetTexture("Assets/Background/Sky_0.png"));
    BGS -> AddImage(this -> GetTexture("Assets/Background/Sky_1.png"));
    BGS -> AddImage(this -> GetTexture("Assets/Background/Sky_2.png"));
    BGS -> setParallax(0.25f);
    backgroundSky -> SetSprite(BGS);
    
    this -> LoadNextLevel();
}

void Game::LoadNextLevel() {
    std::ifstream myfile;
    myfile.open("Assets/Level" + std::to_string(levelNum) + ".txt");
    
    char blockType;
    int x = 32 + levelxpos;
    int y = 16;
    for(int i = 0; i < 24; i++) {
        x = 32 + levelxpos;
        for(int j = 0; j < 56; j++) {
            
            myfile >> blockType;
            
            if(blockType != '.' && blockType != 'P' && blockType != 'O' && blockType != '*') {
                if(blockType == 'A') {
                    Block* newBlock = new Block(this);
                    newBlock -> SetPosition(Vector2(x, y));
                    newBlock -> ChangeTexture("Assets/BlockA.png");
                }
                else if(blockType == 'B') {
                    Block* newBlock = new Block(this);
                    newBlock -> SetPosition(Vector2(x, y));
                    newBlock -> ChangeTexture("Assets/BlockB.png");
                }
                else if(blockType == 'C') {
                    Block* newBlock = new Block(this);
                    newBlock -> SetPosition(Vector2(x, y));
                    newBlock -> ChangeTexture("Assets/BlockC.png");
                }
                else if(blockType == 'D') {
                    Block* newBlock = new Block(this);
                    newBlock -> SetPosition(Vector2(x, y));
                    newBlock -> ChangeTexture("Assets/BlockD.png");
                }
                else if(blockType == 'E') {
                    Block* newBlock = new Block(this);
                    newBlock -> SetPosition(Vector2(x, y));
                    newBlock -> ChangeTexture("Assets/BlockE.png");
                }
            }
            else if(blockType == 'P' && firstLoad) {
                mPlayer = new Player(this);
                mPlayer -> SetPosition(Vector2(x, y));
                firstLoad = false;
            }
            else if(blockType == 'O') {
                BarrelSpawner* newBarrelSpawner = new BarrelSpawner(this);
                newBarrelSpawner -> SetPosition(Vector2(x, y));
            }
            else if(blockType == '*') {
                Coin* newCoin = new Coin(this);
                newCoin -> SetPosition(Vector2(x, y - 16));
            }
            
            x += 64;
        }
        y += 32;
    }
    
    levelNum++;
    if (levelNum > 3) {
        levelNum = 0;
    }
    levelxpos += 56 * 64;
}

void Game::UnloadData() {
  while (actors.size() != 0) {
    delete actors.back();
  }
    for(auto it = textureMap.begin(); it != textureMap.end(); it++) {
        SDL_DestroyTexture(it -> second);
    }
    for(auto it = soundMap.begin(); it != soundMap.end(); it++) {
        Mix_FreeChunk(it -> second);
    }
    textureMap.clear();
    soundMap.clear();
}

void Game::LoadTexture(std::string filename) {
    image = IMG_Load(filename.c_str());
    texture = SDL_CreateTextureFromSurface(renderer, image);
    textureMap.insert(std::pair<std::string, SDL_Texture*>(filename, texture));
    SDL_FreeSurface(image);
}

SDL_Texture* Game::GetTexture(std::string filename) {
    std::unordered_map<std::string, SDL_Texture*>::const_iterator it;
    it = textureMap.find(filename);
    if(it == textureMap.end()) {
        return nullptr;
    }
    else {
        return it -> second;
    }
}

void Game::AddSprite(class SpriteComponent *newSprite) {
    sprites.push_back(newSprite);
    std::sort(sprites.begin(), sprites.end(),
              [](SpriteComponent* a, SpriteComponent* b) {
                  return a->GetDrawOrder() < b->GetDrawOrder();
              });
}

void Game::RemoveSprite(class SpriteComponent *remSprite) {
    auto it = std::find(sprites.begin(), sprites.end(), remSprite);
    sprites.erase(it);
}

void Game::AddBlock(Block *mBlock) {
    blocks.push_back(mBlock);
}

void Game::RemBlock(Block *mBlock) {
    auto it = std::find(blocks.begin(), blocks.end(), mBlock);
    blocks.erase(it);
}

Block* Game::GetBlock(int it) {
    return blocks[it];
}

void Game::LoadSound(const std::string& filename) {
    Mix_Chunk *sample;
    sample = Mix_LoadWAV(filename.c_str());
    soundMap.insert(std::pair<std::string, Mix_Chunk*>(filename, sample));
}

Mix_Chunk* Game::GetSound(const std::string& filename) {
    std::unordered_map<std::string, Mix_Chunk*>::const_iterator it;
    it = soundMap.find(filename);
    if(it == soundMap.end()) {
        return nullptr;
    }
    else {
        return it -> second;
    }
}
