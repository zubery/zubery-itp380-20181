//
//  Block.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Block_hpp
#define Block_hpp

#include <stdio.h>
#include "Actor.h"

class Block : public Actor {
public:
    Block(class Game* mGame);
    ~Block();
};

#endif /* Block_hpp */
