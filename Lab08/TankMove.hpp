//
//  TankMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef TankMove_hpp
#define TankMove_hpp

#include <stdio.h>
#include "MoveComponent.h"
#include "Game.h"

class TankMove : public MoveComponent {
public:
    TankMove(class Actor* mOwner);
    ~TankMove();
    void Update(float deltaTime) override;
    void ProcessInput(const Uint8* keyState) override;
    void SetPlayerTwo();
    
private:
    Uint8 mForwardKey = SDL_SCANCODE_W;
    Uint8 mLeftKey = SDL_SCANCODE_A;
    Uint8 mBackKey = SDL_SCANCODE_S;
    Uint8 mRightKey = SDL_SCANCODE_D;
};


#endif /* TankMove_hpp */
