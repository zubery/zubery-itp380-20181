#include "HUD.h"
#include "Texture.h"
#include "Shader.h"
#include "Game.h"
#include "Renderer.h"
#include "Font.h"
#include <sstream>
#include <iomanip>
#include <iostream>

HUD::HUD(Game* game)
	:mGame(game)
	,mFont(nullptr)
{
	// Load font
	mFont = new Font();
	mFont->Load("Assets/Inconsolata-Regular.ttf");
    coins = 0;
    coinTotal = 55;
    mCoinText = mFont->RenderText(std::to_string(coins) + "/" + std::to_string(coinTotal));
    mTimerText = mFont->RenderText("00:00.00");
    mCheckpointText = mFont -> RenderText("0000");
    time = 0.0f;
    textTime = 0.0f;
}

HUD::~HUD()
{
	// Get rid of font
	if (mFont)
	{
		mFont->Unload();
		delete mFont;
	}
}

void HUD::Update(float deltaTime)
{
	// TODO
    time += deltaTime;
    if(mTimerText != nullptr) {
        mTimerText -> Unload();
        delete mTimerText;
    }
    
    textTime += deltaTime;
    if(textTime >= 5.0f) {
        mCheckpointText = mFont -> RenderText("");
    }
    
    float minutes = time / 60.0f;
    //std::cout << minutes << std::endl;
    std::string mm = std::to_string((int)minutes);
    if(mm.length() < 2) {
        mm = "0" + mm;
    }
    
    float seconds = minutes - (int)minutes;
    seconds *= 60.0f;
    std::string ss = std::to_string((int)seconds);
    if(ss.length() < 2) {
        ss = "0" + ss;
    }
    
    float fraction = seconds - (int)seconds;
    fraction *= 100;
    //std::cout << fraction << std::endl;
    std::string ff = std::to_string((int)fraction);
    if(ff.length() < 2) {
        ff = "0" + ff;
    }
    
    //sprintf();
    
    std::string timestring = mm + ":" + ss + "." + ff;
    mTimerText = mFont->RenderText(timestring);
}

void HUD::Draw(Shader* shader)
{
	// TODO
    DrawTexture(shader, mCoinText, Vector2(-450.0f, -295.0f));
    DrawTexture(shader, mTimerText, Vector2(-420.0f, -325.0f));
    if(mCheckpointText != nullptr) {
        DrawTexture(shader, mCheckpointText, Vector2(0.0f, 0.0f));
    }
}

void HUD::DrawTexture(class Shader* shader, class Texture* texture,
				 const Vector2& offset, float scale)
{
	// Scale the quad by the width/height of texture
	Matrix4 scaleMat = Matrix4::CreateScale(
		static_cast<float>(texture->GetWidth()) * scale,
		static_cast<float>(texture->GetHeight()) * scale,
		1.0f);
	// Translate to position on screen
	Matrix4 transMat = Matrix4::CreateTranslation(
		Vector3(offset.x, offset.y, 0.0f));	
	// Set world transform
	Matrix4 world = scaleMat * transMat;
	shader->SetMatrixUniform("uWorldTransform", world);
	// Set current texture
	texture->SetActive();
	// Draw quad
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

void HUD::CollectCoin() {
    coins++;
    mCoinText = mFont->RenderText(std::to_string(coins) + "/" + std::to_string(coinTotal));
}

void HUD::CheckpointText(std::string display) {
    mCheckpointText = mFont->RenderText(display);
    textTime = 0.0f;
}
