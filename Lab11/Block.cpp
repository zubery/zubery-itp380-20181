//
//  Block.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Block.hpp"
#include "Game.h"
#include "MeshComponent.h"
#include "Renderer.h"
#include "CollisionComponent.h"

Block::Block(Game* mGame) : Actor(mGame) {
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(mGame->GetRenderer()->GetMesh("Assets/Cube.gpmesh"));
    mScale = 64.0f;
    mColl = new CollisionComponent(this);
    mColl -> SetSize(1.0f, 1.0f, 1.0f);
    mGame -> AddBlock(this);
}

Block::~Block() {
    mGame -> RemoveBlock(this);
}
