//
//  Block.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/1/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Block.hpp"
#include <string>
#include "SpriteComponent.h"
#include "Game.h"
#include "CollisionComponent.h"

Block::Block(class Game* mGame)
:Actor(mGame)
{
    mColl = new CollisionComponent(this);
    mColl -> SetSize(64, 32);
    mGame -> AddBlock(this);
}

Block::~Block() {
    mGame -> RemBlock(this);
}

void Block::ChangeTexture(std::string filename) {
    SpriteComponent* newBlockSprite = new SpriteComponent(this);
    newBlockSprite -> SetTexture(this -> mGame -> GetTexture(filename));
    this -> SetSprite(newBlockSprite);
}
