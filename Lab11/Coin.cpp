//
//  Coin.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 4/18/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Coin.hpp"
#include "Game.h"
#include "MeshComponent.h"
#include "Renderer.h"
#include "CollisionComponent.h"
#include "Player.hpp"

Coin::Coin(Game* game) : Actor(game) {
    mMesh = new MeshComponent(this);
    mMesh -> SetMesh(mGame -> GetRenderer() -> GetMesh("Assets/Coin.gpmesh"));
    mColl = new CollisionComponent(this);
    mColl -> SetSize(100.0f, 100.0f, 100.0f);
}

Coin::~Coin() {
    
}

void Coin::UpdateActor(float deltatime) {
    this -> SetRotation((this -> GetRotation()) + (Math::Pi * deltatime));
    
    if(mColl -> Intersect(mGame -> GetPlayer() -> GetCollisionComponent())) {
        Mix_Chunk* mix = mGame -> GetSound("Assets/Sounds/Coin.wav");
        Mix_PlayChannel(-1, mix, 0);
        this -> SetState(State::EDead);
    }
}
