//
//  Checkpoint.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 4/12/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Checkpoint_hpp
#define Checkpoint_hpp

#include <stdio.h>
#include "Actor.h"
#include "Game.h"

class Checkpoint : public Actor {
public:
    Checkpoint(Game* game);
    ~Checkpoint();
    void UpdateActor(float deltaTime) override;
    void MakeActive();
    void SetLevelString(std::string nextLevel) { mLevelString = nextLevel; }
    std::string GetLevelString() { return mLevelString; }
private:
    bool active;
    std::string mLevelString;
};

#endif /* Checkpoint_hpp */
