#include "MoveComponent.h"
#include "Actor.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
    mOwner = owner;
}

void MoveComponent::Update(float deltaTime)
{
	// TODO: Implement in Part 3
    mOwner -> SetRotation((mOwner -> GetRotation()) + (mAngularSpeed * deltaTime));
    mOwner -> SetPosition(Vector3(((mOwner -> GetForward()).x * mForwardSpeed * deltaTime) + (mOwner -> GetPosition()).x, ((mOwner -> GetForward()).y * mForwardSpeed * deltaTime) + (mOwner -> GetPosition()).y, ((mOwner -> GetForward()).z * mForwardSpeed * deltaTime) + (mOwner -> GetPosition()).z));
}
