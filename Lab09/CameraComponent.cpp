//
//  CameraComponent.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "CameraComponent.hpp"
#include "Actor.h"
#include "Game.h"
#include "Renderer.h"

CameraComponent::CameraComponent(Actor* owner) : Component(owner) {
    mPitchAngle = 0.0f;
    mPitchSpeed = 0.0f;
}

CameraComponent::~CameraComponent() {
    
}

void CameraComponent::Update(float deltaTime) {
    mPitchAngle += deltaTime * mPitchSpeed;
    if(mPitchAngle < -(Math::PiOver2 / 2.0f)) {
        mPitchAngle = -(Math::PiOver2 / 2.0f);
    }
    if(mPitchAngle > (Math::PiOver2 / 2.0f)) {
        mPitchAngle = (Math::PiOver2 / 2.0f);
    }
    
    Matrix4 pitchMatrix = Matrix4::CreateRotationY(mPitchAngle);
    Matrix4 yawMatrix = Matrix4::CreateRotationZ(mOwner -> GetRotation());
    Matrix4 combinationMatrix = pitchMatrix * yawMatrix;
    Vector3 transformCombination = Vector3::Transform(Vector3(1, 0, 0), combinationMatrix);
    
    Matrix4 mLook = Matrix4::CreateLookAt(mOwner -> GetPosition(), mOwner -> GetPosition() + (2.0f * transformCombination), Vector3(0.0f, 0.0f, 1.0f));
    mOwner -> GetGame() -> GetRenderer() -> SetViewMatrix(mLook);
}
