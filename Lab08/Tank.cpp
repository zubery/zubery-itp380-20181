//
//  Tank.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Tank.hpp"
#include "Game.h"
#include "MeshComponent.h"
#include "Renderer.h"
#include "TankMove.hpp"
#include "Turret.hpp"
#include "CollisionComponent.h"
#include "Bullet.hpp"

Tank::Tank(Game* mGame) : Actor(mGame) {
    mMesh = new MeshComponent(this);
    mMesh -> SetMesh(mGame->GetRenderer()->GetMesh("Assets/TankBase.gpmesh"));
    mComp = new TankMove(this);
    mTurret = new Turret(mGame);
    mColl = new CollisionComponent(this);
    mColl -> SetSize(30.0f, 30.0f, 30.0f);
    pressed = false;
}

Tank::~Tank() {
}

void Tank::UpdateActor(float deltaTime) {
    mTurret -> SetPosition(this -> GetPosition());
}

void Tank::SetPlayerTwo() {
    TankMove* tankMove = static_cast<TankMove*>(mComp);
    tankMove -> SetPlayerTwo();
    mTurret -> SetPlayerTwo();
    mMesh -> SetTextureIndex(1);
    fireKey = SDL_SCANCODE_RSHIFT;
}

void Tank::Fire() {
    Bullet* fired = new Bullet(mGame);
    fired -> SetShot(this);
    fired -> SetPosition(this -> GetPosition());
    fired -> SetRotation(mTurret -> GetRotation());
}

void Tank::Respawn() {
    this -> SetPosition(initialPos);
    this -> SetRotation(0.0f);
    mTurret -> SetRotation(0.0f);
}

void Tank::ActorInput(const Uint8 *keyState) {
    if(keyState[fireKey] && !pressed) {
        this -> Fire();
    }
    
    pressed = keyState[fireKey];
}
