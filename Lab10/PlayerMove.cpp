//
//  PlayerMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PlayerMove.hpp"
#include "Game.h"
#include "Actor.h"
#include "CameraComponent.hpp"
#include "CollisionComponent.h"
#include "Block.hpp"
#include <iostream>

PlayerMove::PlayerMove(Actor* owner) : MoveComponent(owner) {
    this -> ChangeState(MoveState::Falling);
    mMass = 1.0f;
    mWallRunTimer = 0.0f;
    mGravity = Vector3(0.0f, 0.0f, -980.0f);
    mJumpForce = Vector3(0.0f, 0.0f, 35000.0f);
    mWallForce = Vector3(0.0f, 0.0f, 1800.0f);
    mWallRunForce = Vector3(0.0f, 0.0f, 1200.0f);
}

PlayerMove::~PlayerMove() {
    
}

void PlayerMove::Update(float deltatime) {
    if(mCurrentState == MoveState::OnGround) {
        this -> UpdateOnGround(deltatime);
    }
    else if(mCurrentState == MoveState::Falling) {
        this -> UpdateFalling(deltatime);
    }
    else if(mCurrentState == MoveState::Jump) {
        this -> UpdateJump(deltatime);
    }
    else if(mCurrentState == MoveState::WallClimb) {
        this -> UpdateWallClimb(deltatime);
    }
    else if(mCurrentState == MoveState::WallRun) {
        this -> UpdateWallRun(deltatime);
    }
}

void PlayerMove::ProcessInput(const Uint8 *keyState) {
    int x, y;
    SDL_GetRelativeMouseState(&x, &y);
    
    float xAngular;
    xAngular = x / 500.0f;
    xAngular *= (M_PI * 10.0f);
    this -> SetAngularSpeed(xAngular);
    
    float yAngular;
    yAngular = y / 500.0f;
    yAngular *= (M_PI * 10.0f);
    this -> mOwner -> GetCamera() -> SetPitchSpeed(yAngular);
    
    if(keyState[SDL_SCANCODE_W] && keyState[SDL_SCANCODE_S]) {
        this -> AddForce(Vector3(0.0f, 0.0f, 0.0f));
    }
    else if(keyState[SDL_SCANCODE_W]) {
        this -> AddForce(mOwner -> GetForward() * 700.0f);
    }
    else if(keyState[SDL_SCANCODE_S]) {
        this -> AddForce(mOwner -> GetForward() * -700.0f);
    }
    else {
        this -> AddForce(Vector3(0.0f, 0.0f, 0.0f));
    }
    
    if(keyState[SDL_SCANCODE_A] && keyState[SDL_SCANCODE_D]) {
        this -> AddForce(Vector3(0.0f, 0.0f, 0.0f));
    }
    else if(keyState[SDL_SCANCODE_A]) {
        this -> AddForce(mOwner -> GetRight() * -700.0f);
    }
    else if(keyState[SDL_SCANCODE_D]) {
        this -> AddForce(mOwner -> GetRight() * 700.0f);
    }
    else {
        this -> AddForce(Vector3(0.0f, 0.0f, 0.0f));
    }
    
    if(!spacePressed && keyState[SDL_SCANCODE_SPACE]) {
        if(mCurrentState == MoveState::OnGround) {
            this -> AddForce(mJumpForce);
            this -> ChangeState(PlayerMove::MoveState::Jump);
            spacePressed = true;
        }
    }
    else if (!keyState[SDL_SCANCODE_SPACE]) {
        spacePressed = false;
    }
}

void PlayerMove::UpdateJump(float deltatime) {
    AddForce(mGravity);
    PhysicsUpdate(deltatime);
    
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        if(check == PlayerMove::CollSide::Bottom) {
            mVelocity.z = 0.0f;
        }
        
        bool climbwall = false;
        bool runwall = false;
        if(check == PlayerMove::CollSide::SideX1 || check == PlayerMove::CollSide::SideX2 || check == PlayerMove::CollSide::SideY1 || PlayerMove::CollSide::SideY2) {
            climbwall = this -> CanWallClimb(check);
            if(climbwall == true) {
                this -> ChangeState(PlayerMove::MoveState::WallClimb);
                mWallClimbTimer = 0.0f;
                i = mOwner -> GetGame() -> GetNumBlocks();
            }
            runwall = this -> CanWallRun(check);
            if(runwall == true) {
                this -> ChangeState(PlayerMove::MoveState::WallRun);
                mWallClimbTimer = 0.0f;
                i = mOwner -> GetGame() -> GetNumBlocks();
            }
        }
    }
    if(mVelocity.z <= 0.0f) {
        this -> ChangeState(PlayerMove::MoveState::Falling);
    }
    
}

void PlayerMove::UpdateFalling(float deltatime) {
    AddForce(mGravity);
    PhysicsUpdate(deltatime);
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        if(check == PlayerMove::CollSide::Top) {
            mVelocity.z = 0.0f;
            this -> ChangeState(PlayerMove::MoveState::OnGround);
        }
    }
}

void PlayerMove::UpdateOnGround(float deltatime) {
    bool fall = true;
    //static int counter = 0;
    PhysicsUpdate(deltatime);
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        if(check == PlayerMove::CollSide::Top) {
            fall = false;
        }
        
        bool climbwall = false;
        if(check == PlayerMove::CollSide::SideX1 || check == PlayerMove::CollSide::SideX2 || check == PlayerMove::CollSide::SideY1 || PlayerMove::CollSide::SideY2) {
            climbwall = this -> CanWallClimb(check);
            if(climbwall == true) {
                this -> ChangeState(PlayerMove::MoveState::WallClimb);
                mWallClimbTimer = 0.0f;
                fall = false;
                i = mOwner -> GetGame() -> GetNumBlocks();
            }
        }
    }
    if(fall) {
        this -> ChangeState(PlayerMove::MoveState::Falling);
    }
}

void PlayerMove::UpdateWallClimb(float deltatime) {
    bool nocollision = true;
    
    this -> AddForce(mGravity);
    if(mWallClimbTimer < 0.4f) {
        this -> AddForce(mWallForce);
    }
    this -> PhysicsUpdate(deltatime);
    
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        PlayerMove::CollSide check = this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
        
        if(check != PlayerMove::CollSide::None) {
            nocollision = false;
        }
    }
    
    if(nocollision || mVelocity.z <= 0.0f) {
        mVelocity.z = 0.0f;
        this -> ChangeState(PlayerMove::MoveState::Falling);
    }
    
    mWallClimbTimer += deltatime;
}

void PlayerMove::UpdateWallRun(float deltatime) {
    
    this -> AddForce(mGravity);
    if(mWallRunTimer < 0.4f) {
        this -> AddForce(mWallRunForce);
    }
    this -> PhysicsUpdate(deltatime);
    
    for(int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        this -> FixCollision(mOwner -> GetCollisionComponent(), currBlock -> GetCollisionComponent());
    }
    
    if(mVelocity.z <= 0.0f) {
        mVelocity.z = 0.0f;
        this -> ChangeState(PlayerMove::MoveState::Falling);
    }
    
    mWallRunTimer += deltatime;

}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent *self, CollisionComponent *block) {
    PlayerMove::CollSide collisionSide = PlayerMove::CollSide::None;
    
    float newX = mOwner -> GetPosition().x;
    float newY = mOwner -> GetPosition().y;
    float newZ = mOwner -> GetPosition().z;
    
    mOwner -> SetPosition(Vector3(newX, newY, newZ));
    
    float playerYMin = (self -> GetMin().y);
    float playerYMax = (self -> GetMax().y);
    float playerXMin = (self -> GetMin().x);
    float playerXMax = (self -> GetMax().x);
    float playerZMin = (self -> GetMin().z);
    float playerZMax = (self -> GetMax().z);
    
    if (self -> Intersect(block)) {
        float dx1, dx2, dy1, dy2, dz1, dz2;
        float tempx1, tempx2, tempy1, tempy2, tempz1, tempz2;
        dx2 = (block-> GetMax().x) - playerXMin;
        dx1 = (block -> GetMin().x) - playerXMax;
        dy1 = (block -> GetMin().y) - playerYMax;
        dy2 = (block -> GetMax().y) - playerYMin;
        dz1 = (block -> GetMin().z) - playerZMax;
        dz2 = (block -> GetMax().z) - playerZMin;
            
        tempx2 = dx2;
        tempx1 = dx1;
        tempy2 = dy2;
        tempy1 = dy1;
        tempz1 = dz1;
        tempz2 = dz2;
            
        if(dx2 < 0) {
            tempx2 = dx2 * -1;
        }
        if(dx1 < 0) {
            tempx1 = dx1 * -1;
        }
        if(dy2 < 0) {
            tempy2 = dy2 * -1;
        }
        if(dy1 < 0) {
            tempy1 = dy1 * -1;
        }
        if(dz1 < 0) {
            tempz1 = dz1 * -1;
        }
        if(dz2 < 0) {
            tempz2 = dz2 * -1;
        }
            
        float smallest = std::min(tempz1, std::min(tempz2, std::min(tempx1, std::min(tempx2, std::min(tempy1, tempy2)))));
            
        if(smallest == tempy1) {
            newY += dy1;
            playerYMin += dy1;
            playerYMax += dy1;
            collisionSide = PlayerMove::CollSide::SideY1;
            this -> AddForce(Vector3(0.0f, -700.0f, 0.0f));
        }
        else if(smallest == tempy2) {
            newY += dy2;
            playerYMin += dy2;
            playerYMax += dy2;
            collisionSide = PlayerMove::CollSide::SideY2;
            this -> AddForce(Vector3(0.0f, 700.0f, 0.0f));
        }
        else if(smallest == tempx1) {
            newX += dx1;
            playerXMin += dx1;
            playerXMax += dx1;
            collisionSide = PlayerMove::CollSide::SideX1;
            this -> AddForce(Vector3(-700.0f, 0.0f, 0.0f));
        }
        else if(smallest == tempx2) {
            newX += dx2;
            playerXMin += dx2;
            playerXMax += dx2;
            collisionSide = PlayerMove::CollSide::SideX2;
            this -> AddForce(Vector3(700.0f, 0.0f, 0.0f));
        }
        else if(smallest == tempz1) {
            newZ += dz1;
            playerZMin += dz1;
            playerZMax += dz1;
            collisionSide = PlayerMove::CollSide::Bottom;
        }
        else if(smallest == tempz2) {
            newZ += dz2;
            playerZMin += dz2;
            playerZMax += dz2;
            collisionSide = PlayerMove::CollSide::Top;
        }
    }
    
    mOwner -> SetPosition(Vector3(newX, newY, newZ));
    return collisionSide;
}

void PlayerMove::PhysicsUpdate(float deltatime) {
    mAcceleration = mPendingForces * (1.0f / mMass);
    mVelocity += mAcceleration * deltatime;
    this -> FixXYVelocity();
    mOwner -> SetPosition(mOwner -> GetPosition() + (mVelocity * deltatime));
    mOwner -> SetRotation(mOwner -> GetRotation() + (this -> GetAngularSpeed() * deltatime));
    mPendingForces = Vector3::Zero;
}

void PlayerMove::AddForce(const Vector3 &force) {
    mPendingForces += force;
}

void PlayerMove::FixXYVelocity() {
    Vector2 xyVelocity = Vector2(mVelocity.x, mVelocity.y);
    if(xyVelocity.Length() > 400.0f) {
        xyVelocity.Normalize();
        xyVelocity *= 400.0f;
    }
    if(mCurrentState == MoveState::OnGround || mCurrentState == MoveState::WallClimb) {
        if(Math::NearZero(mAcceleration.x)) {
            xyVelocity.x *= 0.9f;
        }
        if(Math::NearZero(mAcceleration.y)) {
            xyVelocity.y *= 0.9f;
        }
        if((xyVelocity.x < 0 && mAcceleration.x > 0) || (xyVelocity.x > 0 && mAcceleration.x < 0)) {
            xyVelocity.x *= 0.9f;
        }
        if((xyVelocity.y < 0 && mAcceleration.y > 0) || (xyVelocity.y > 0 && mAcceleration.y < 0)) {
            xyVelocity.y *= 0.9f;
        }
    }
    
    mVelocity.x = xyVelocity.x;
    mVelocity.y = xyVelocity.y;
}

bool PlayerMove::CanWallClimb(PlayerMove::CollSide currentSide) {
    Vector3 normal(0.0f, 0.0f, 0.0f);
    if(currentSide == PlayerMove::CollSide::SideX1) {
        normal = Vector3(1.0f, 0.0f, 0.0f);
    }
    else if(currentSide == PlayerMove::CollSide::SideX2) {
        normal = Vector3(-1.0f, 0.0f, 0.0f);
    }
    else if(currentSide == PlayerMove::CollSide::SideY1) {
        normal = Vector3(0.0f, 1.0f, 0.0f);
    }
    else if(currentSide == PlayerMove::CollSide::SideY2) {
        normal = Vector3(0.0f, -1.0f, 0.0f);
    }
    else {
        return false;
    }
    
    float dotForward = Vector3::Dot(normal, mOwner -> GetForward());
    float forwardAngle = Math::Acos(dotForward) * (180.0f / Math::Pi);
    //std::cout << "forward " << forwardAngle << std::endl;
    
    if(forwardAngle < 30.0f && forwardAngle > -30.0f) {
        if(mVelocity.Length() > 350.0f) {
            float length = mVelocity.Length();
            float dotVelocity = Vector3::Dot(normal, Vector3(mVelocity.x / length, mVelocity.y / length, mVelocity.z / length));
            float velocityAngle = Math::Acos(dotVelocity) * (180.0f / Math::Pi);
            //std::cout << "velocity " << velocityAngle << std::endl;
            if(velocityAngle < 30.0f && velocityAngle > -30.0f) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            mVelocity.x = 0.0f;
            mVelocity.y = 0.0f;
            return false;
        }
    }
    else {
        return false;
    }
}

bool PlayerMove::CanWallRun(PlayerMove::CollSide currentSide) {
    Vector3 normal(0.0f, 0.0f, 0.0f);
    if(currentSide == PlayerMove::CollSide::SideX1) {
        normal = Vector3(1.0f, 0.0f, 0.0f);
    }
    else if(currentSide == PlayerMove::CollSide::SideX2) {
        normal = Vector3(-1.0f, 0.0f, 0.0f);
    }
    else if(currentSide == PlayerMove::CollSide::SideY1) {
        normal = Vector3(0.0f, 1.0f, 0.0f);
    }
    else if(currentSide == PlayerMove::CollSide::SideY2) {
        normal = Vector3(0.0f, -1.0f, 0.0f);
    }
    else {
        return false;
    }
    
    float dotForward = Vector3::Dot(normal, mOwner -> GetForward());
    float forwardAngle = Math::Acos(dotForward) * (180.0f / Math::Pi);
    std::cout << "forward " << forwardAngle << std::endl;
    
    if(forwardAngle < 135.0f && forwardAngle > 45.0f) {
        if(mVelocity.Length() > 350.0f) {
            float length = mVelocity.Length();
            float dotVelocity = Vector3::Dot(mOwner -> GetForward(), Vector3(mVelocity.x / length, mVelocity.y / length, mVelocity.z / length));
            float velocityAngle = Math::Acos(dotVelocity) * (180.0f / Math::Pi);
            //std::cout << "velocity " << velocityAngle << std::endl;
            if(velocityAngle < 30.0f && velocityAngle > -30.0f) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            mVelocity.x = 0.0f;
            mVelocity.y = 0.0f;
            return false;
        }
    }
    else {
        return false;
    }
}
