//
//  PlayerMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PlayerMove_hpp
#define PlayerMove_hpp

#include <stdio.h>
#include "MoveComponent.h"

class PlayerMove : public MoveComponent {
public:
    PlayerMove(class Actor* owner);
    ~PlayerMove();
    void Update(float deltatime) override;
    void ProcessInput(const Uint8* keyState) override;
    enum MoveState {OnGround, Jump, Falling};
    enum CollSide {None, Top, Bottom, Side};
    void ChangeState(MoveState newState) { mCurrentState = newState; }
protected:
    CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
    void UpdateOnGround(float deltatime);
    void UpdateJump(float deltatime);
    void UpdateFalling(float deltatime);
private:
    MoveState mCurrentState;
    float mZSpeed;
    const float Gravity = -980.0f;
    const float JumpSpeed = 500.0f;
    bool spacePressed = false;
};

#endif /* PlayerMove_hpp */
