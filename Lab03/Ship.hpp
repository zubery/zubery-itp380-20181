//
//  Ship.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 1/31/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Ship_hpp
#define Ship_hpp

#include <stdio.h>
#include "Actor.h"
#include "SpriteComponent.h"
#include "MoveComponent.h"

class Ship : public Actor {
public:
    Ship(class Game* mGame);
    ~Ship();
    
    virtual void ActorInput(const Uint8* keyState) override;
    
};

#endif /* Ship_hpp */
