//
//  PlaneMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PlaneMove.hpp"
#include "Actor.h"
#include "Tile.h"
#include "Grid.h"
#include "Game.h"
#include <iostream>

PlaneMove::PlaneMove(Actor* mOwner) : MoveComponent(mOwner) {
    this -> SetForwardSpeed(5.0f);
    this -> SetNextTile(mOwner -> GetGame() -> getGrid() -> GetStartTile() -> GetParent());
}
    
PlaneMove::~PlaneMove() {
    
}

void PlaneMove::Update(float deltaTime) {
    MoveComponent::Update(deltaTime);
    
    if(mNextTile != nullptr) {
        float diffx = mNextTile -> GetPosition().x - mOwner -> GetPosition().x;
        float diffy = mNextTile -> GetPosition().y - mOwner -> GetPosition().y;
        if (diffx + diffy < 2.0f) {
            this -> SetNextTile(mNextTile -> GetParent());
        }
    }
    if(mOwner -> GetPosition().x > 1024) {
        mOwner -> SetState(Actor::EDead);
    }
}

void PlaneMove::SetNextTile(const class Tile *passedTile) {
    mNextTile = passedTile;
    if(mNextTile == nullptr) {
        mOwner -> SetRotation(0.0f);
    }
    else {
        Vector2 angle = (mNextTile -> GetPosition()) - (mOwner -> GetPosition());
        mOwner -> SetRotation(atan2((angle.y * -1.0f), angle.x));
    }
}
