//
//  Tower.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Tower.hpp"
#include "Game.h"
#include "SpriteComponent.h"
#include "Plane.hpp"
#include "Bullet.hpp"

Tower::Tower(Game* mGame) : Actor(mGame) {
    mSprite = new SpriteComponent(this, 200);
    mSprite -> SetTexture(mGame -> GetTexture("Assets/Tower.png"));
}

Tower::~Tower() {
    
}

void Tower::UpdateActor(float deltaTime) {
    attackTimer -= deltaTime;
    if(attackTimer <= 0.0f) {
        Plane* shoot = this -> ClosestPlane();
        float distance = abs((shoot -> GetPosition().x) - (this -> GetPosition().x)) + abs((shoot -> GetPosition().y) - (this -> GetPosition().y));
        
        if(distance < 250.0f) {
            Vector2 angle = (shoot -> GetPosition()) - (this -> GetPosition());
            this -> SetRotation(atan2(-1 * angle.y, angle.x));
            
            Bullet* shot = new Bullet(mGame);
            shot -> SetPosition(this -> GetPosition());
            shot -> SetRotation(this -> GetRotation());
        }
        attackTimer = 4.0f;
    }
}

Plane* Tower::ClosestPlane() {
    std::vector<Plane*> myPlanes = *(mGame -> getPlanes());
    
    if (myPlanes.size() == 0) {
        return nullptr;
    }
    else {
        int smallestIndex = 0;
        float smallest = abs((myPlanes[0] -> GetPosition().x) - (this -> GetPosition()).x) + abs((myPlanes[0] -> GetPosition().y) - (this -> GetPosition()).y);
        
        for(int i = 0; i < myPlanes.size(); i++) {
            float current = abs((myPlanes[i] -> GetPosition().x) - (this -> GetPosition()).x) + abs((myPlanes[i] -> GetPosition().y) - (this -> GetPosition()).y);
            if(smallest > current) {
                smallestIndex = i;
                smallest = current;
            }
        }
        
        return myPlanes[smallestIndex];
    }
}

