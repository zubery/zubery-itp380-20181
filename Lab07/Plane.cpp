//
//  Plane.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Plane.hpp"
#include "Game.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "Grid.h"
#include "Tile.h"
#include "MoveComponent.h"
#include "PlaneMove.hpp"

Plane::Plane(Game* mGame) : Actor(mGame) {
    this -> SetPosition(mGame -> getGrid() -> GetStartTile() -> GetPosition());
    mSprite = new SpriteComponent(this, 200);
    mSprite -> SetTexture(mGame -> GetTexture("Assets/Airplane.png"));
    mColl = new CollisionComponent(this);
    mColl -> SetSize(64.0f, 64.0f);
    mGame -> addPlane(this);
    mComp = new PlaneMove(this);
}

Plane::~Plane() {
    mGame -> removePlane(this);
}
