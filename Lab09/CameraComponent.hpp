//
//  CameraComponent.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef CameraComponent_hpp
#define CameraComponent_hpp

#include <stdio.h>
#include "Component.h"

class CameraComponent : public Component {
public:
    CameraComponent(class Actor* owner);
    ~CameraComponent();
    void Update(float deltaTime) override;
    float GetPitchSpeed() { return mPitchSpeed; }
    void SetPitchSpeed(float newPitchSpeed) { mPitchSpeed = newPitchSpeed; }
private:
    float mPitchAngle;
    float mPitchSpeed;
};

#endif /* CameraComponent_hpp */
