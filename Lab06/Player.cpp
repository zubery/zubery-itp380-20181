//
//  Player.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Player.hpp"
#include "SpriteComponent.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "PlayerMove.hpp"
#include "AnimatedSprite.hpp"
#include <string>

Player::Player(Game* mGame) : Actor(mGame) {
    SpriteComponent* newSprite = new AnimatedSprite(this, 200);
    
    AnimatedSprite* tempSprite = static_cast<AnimatedSprite*>(newSprite);
    for (int i = 0; i < 10; i++) {
        tempSprite -> AddImage(mGame -> GetTexture("Assets/Player/Run" + std::to_string(i + 1) + ".png"));
    }
    
    mSprite = newSprite;
    mColl = new CollisionComponent(this);
    mColl -> SetSize(20.0f, 64.0f);
    mComp = new PlayerMove(this);
    initialX = this -> GetPosition().x;
    initialY = this -> GetPosition().y;
}

Player::~Player() {
    
}
