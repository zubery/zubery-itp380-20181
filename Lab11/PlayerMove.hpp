//
//  PlayerMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PlayerMove_hpp
#define PlayerMove_hpp

#include <stdio.h>
#include "MoveComponent.h"
#include "Math.h"

class PlayerMove : public MoveComponent {
public:
    PlayerMove(class Actor* owner);
    ~PlayerMove();
    void Update(float deltatime) override;
    void ProcessInput(const Uint8* keyState) override;
    enum MoveState {OnGround, Jump, Falling, WallClimb, WallRun};
    enum CollSide {None, Top, Bottom, Side, SideX1, SideX2, SideY1, SideY2};
    void ChangeState(MoveState newState) { mCurrentState = newState; }
    void PhysicsUpdate(float deltatime);
    void AddForce(const Vector3& force);
    void FixXYVelocity();
    bool CanWallClimb(CollSide currentSide);
    bool CanWallRun(CollSide currentSide);
    void Pause();
protected:
    CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
    void UpdateOnGround(float deltatime);
    void UpdateJump(float deltatime);
    void UpdateFalling(float deltatime);
    void UpdateWallClimb(float deltatime);
    void UpdateWallRun(float deltatime);
private:
    int mRunningSFX; 
    MoveState mCurrentState;
    bool spacePressed = false;
    float mMass;
    float mWallClimbTimer;
    float mWallRunTimer;
    Vector3 mVelocity;
    Vector3 mAcceleration;
    Vector3 mPendingForces;
    Vector3 mGravity;
    Vector3 mJumpForce;
    Vector3 mWallForce;
    Vector3 mWallRunForce;
};

#endif /* PlayerMove_hpp */
