//
//  Bullet.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Bullet_hpp
#define Bullet_hpp

#include <stdio.h>
#include "Actor.h"

class Bullet : public Actor {
public:
    Bullet(class Game* mGame);
    ~Bullet();
    void UpdateActor(float deltaTime) override;
private:
    float lifespan = 3.0f;
};

#endif /* Bullet_hpp */
