//
//  AnimatedSprite.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/27/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "AnimatedSprite.hpp"

AnimatedSprite::AnimatedSprite(Actor* owner, int drawOrder) : SpriteComponent(owner, drawOrder) {
    
}

AnimatedSprite::~AnimatedSprite() {
    
}

void AnimatedSprite::Update(float deltaTime) {
    mAnimTimer += mAnimSpeed * deltaTime;
    int frame = static_cast<int>(mAnimTimer);
    if(frame < mImages.size()) {
        this -> SetTexture(mImages[frame]);
    }
    else {
        mAnimTimer = 0.0f;
    }
}
