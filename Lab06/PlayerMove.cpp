//
//  PlayerMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PlayerMove.hpp"
#include "MoveComponent.h"
#include "Actor.h"
#include "Block.hpp"
#include "CollisionComponent.h"
#include <algorithm>
#include <SDL/SDL_mixer.h>

PlayerMove::PlayerMove(Actor* mOwner) : MoveComponent(mOwner) {
    mYSpeed = 0.0f;
    mSpacePressed = false;
    mInAir = false;
    this -> SetForwardSpeed(125.0f);
}

PlayerMove::~PlayerMove() {
    
}

void PlayerMove::Update(float deltaTime) {
    float newX = mOwner -> GetPosition().x + (this -> GetForwardSpeed() * deltaTime);
    float newY = mOwner -> GetPosition().y + (mYSpeed * deltaTime);
    
    mOwner -> SetPosition(Vector2(newX, newY));
    
    if(newY >= 768.0f) {
        newY = 768.0f;
        mInAir = false;
    }
    
    float playerYMin = (mOwner -> GetCollisionComponent() -> GetMin().y);
    float playerYMax = (mOwner -> GetCollisionComponent() -> GetMax().y);
    float playerXMin = (mOwner -> GetCollisionComponent() -> GetMin().x);
    float playerXMax = (mOwner -> GetCollisionComponent() -> GetMax().x);
    
    for (int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = mOwner -> GetGame() -> GetBlock(i);
        if ((mOwner -> GetCollisionComponent()) -> Intersect(currBlock -> GetCollisionComponent())) {
            float dx1, dx2, dy1, dy2;
            float tempx1, tempx2, tempy1, tempy2;
            dx2 = (currBlock -> GetCollisionComponent() -> GetMax().x) - playerXMin;
            dx1 = (currBlock -> GetCollisionComponent() -> GetMin().x) - playerXMax;
            dy1 = (currBlock -> GetCollisionComponent() -> GetMin().y) - playerYMax;
            dy2 = (currBlock -> GetCollisionComponent() -> GetMax().y) - playerYMin;
            
            tempx2 = dx2;
            tempx1 = dx1;
            tempy2 = dy2;
            tempy1 = dy1;
            
            if(dx2 < 0) {
                tempx2 = dx2 * -1;
            }
            if(dx1 < 0) {
                tempx1 = dx1 * -1;
            }
            if(dy2 < 0) {
                tempy2 = dy2 * -1;
            }
            if(dy1 < 0) {
                tempy1 = dy1 * -1;
            }
            
            float smallest = std::min(tempx1, std::min(tempx2, std::min(tempy1, tempy2)));
            
            if(smallest == tempy1) {
                newY += dy1;
                playerYMin += dy1;
                playerYMax += dy1;
                mYSpeed = 0.0f;
                mInAir = false;
            }
            if(smallest == tempy2) {
                newY += dy2;
                playerYMin += dy2;
                playerYMax += dy2;
                if(mYSpeed < 0) {
                    mYSpeed = 0.0f;
                }
            }
            if(smallest == tempx1) {
                newX += dx1;
                playerXMin += dx1;
                playerXMax += dx1;
            }
            if(smallest == tempx2) {
                newX += dx2;
                playerXMin += dx2;
                playerXMax += dx2;
            }
        }
    }
    
    mOwner -> SetPosition(Vector2(newX, newY));
    if(newX >= 512) {
        mOwner -> GetGame() -> SetCameraPos(Vector2(newX - 512, 0));
    }
    mYSpeed += (600.0f * deltaTime);
    
    float maxSpeed = 400.0f;
    
    if (mYSpeed > maxSpeed) {
        mYSpeed = maxSpeed;
    }
    
    if(mSpacePressed) {
        if(mJumpTimer > 0.0f) {
            mYSpeed -= (950.0f * deltaTime);
            mJumpTimer -= deltaTime;
        }
    }
    else {
        if (!mInAir) {
            mJumpTimer = 0.3f;
        }
        else {
            mJumpTimer = 0.0f;
        }
    }
    
    if(mOwner -> GetGame() -> GetCameraPos().x >= ((mOwner -> GetGame() -> getLevelXPos()) - 1024)) {
        mOwner -> GetGame() -> LoadNextLevel();
    }
}

void PlayerMove::ProcessInput(const Uint8 *keyState) {
    //FAT!!!!!
    if(!mSpacePressed && keyState[SDL_SCANCODE_SPACE]) {
        if(!mInAir) {
            mYSpeed = -300.0f;
            Mix_Chunk *chunk = mOwner -> GetGame() -> GetSound("Assets/Player/Jump.wav");
            Mix_PlayChannel(-1, chunk, 0);
            mInAir = true;
        }
        
    }
    
    mSpacePressed = keyState[SDL_SCANCODE_SPACE];
}
