//
//  BarrelSpawner.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "BarrelSpawner.hpp"
#include "Barrel.hpp"

BarrelSpawner::BarrelSpawner(Game* mGame) : Actor(mGame) {
    timer = 3.0f;
}

BarrelSpawner::~BarrelSpawner() {
    
}

void BarrelSpawner::UpdateActor(float deltaTime) {
    timer -= deltaTime;
    if (timer <= 0.0f) {
        Barrel* newBarrel = new Barrel(mGame);
        float x = this -> GetPosition().x;
        float y = this -> GetPosition().y;
        newBarrel -> SetPosition(Vector2(x, y));
        
        timer = 3.0f;
    }
}
