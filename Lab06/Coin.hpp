//
//  Coin.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/27/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Coin_hpp
#define Coin_hpp

#include <stdio.h>
#include "Actor.h"

class Coin : public Actor {
public:
    Coin(class Game* mGame);
    ~Coin();
    void UpdateActor(float deltaTime) override;
};

#endif /* Coin_hpp */
