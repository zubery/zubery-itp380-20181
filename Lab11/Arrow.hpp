//
//  Arrow.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 4/18/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Arrow_hpp
#define Arrow_hpp

#include <stdio.h>
#include "Actor.h"

class Arrow : public Actor {
public:
    Arrow(class Game* game);
    ~Arrow();
    void UpdateActor(float deltatime) override; 
};

#endif /* Arrow_hpp */
