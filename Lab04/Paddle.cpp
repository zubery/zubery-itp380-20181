//
//  Paddle.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/1/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Paddle.hpp"
#include "SpriteComponent.h"
#include "Game.h"
#include "MoveComponent.h"
#include "PaddleMove.hpp"
#include "CollisionComponent.h"

Paddle::Paddle(class Game* mGame) : Actor(mGame) {
    mSprite = new SpriteComponent(this);
    mSprite -> SetTexture(mGame -> GetTexture("Assets/Paddle.png"));
    mComp = new PaddleMove(this);
    mColl = new CollisionComponent(this);
    mColl -> SetSize(104, 24);
    this -> SetPosition(Vector2(512, 730));
}

Paddle::~Paddle() {
    
}

void Paddle::ActorInput(const Uint8 *keyState) {
    if(keyState[SDL_SCANCODE_RIGHT]) {
        mComp -> SetForwardSpeed(7);
    }
    else if(keyState[SDL_SCANCODE_LEFT]) {
        mComp -> SetForwardSpeed(-7);
    }
    else {
        mComp -> SetForwardSpeed(0);
    }
}
