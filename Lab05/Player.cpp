//
//  Player.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Player.hpp"
#include "SpriteComponent.h"
#include "Game.h"
#include "CollisionComponent.h"
#include "PlayerMove.hpp"

Player::Player(Game* mGame) : Actor(mGame) {
    SpriteComponent* newSprite = new SpriteComponent(this, 200);
    newSprite -> SetTexture(mGame -> GetTexture("Assets/Player/Idle.png"));
    
    mSprite = newSprite;
    mColl = new CollisionComponent(this);
    mColl -> SetSize(20.0f, 64.0f);
    mComp = new PlayerMove(this);
    initialX = this -> GetPosition().x;
    initialY = this -> GetPosition().y;
}

Player::~Player() {
    
}
