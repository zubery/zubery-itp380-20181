//
//  PlaneMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PlaneMove_hpp
#define PlaneMove_hpp

#include <stdio.h>
#include "MoveComponent.h"

class PlaneMove : public MoveComponent {
public:
    PlaneMove(class Actor* mOwner);
    ~PlaneMove();
    void Update(float deltaTime) override;
    void SetNextTile(const class Tile* passedTile);
    
private:
    const class Tile* mNextTile;
};


#endif /* PlaneMove_hpp */
