//
//  Turret.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Turret.hpp"
#include "MeshComponent.h"
#include "Renderer.h"
#include "MoveComponent.h"

Turret::Turret(Game* mGame) : Actor(mGame) {
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(mGame->GetRenderer()->GetMesh("Assets/TankTurret.gpmesh"));
    mComp = new MoveComponent(this);
    mScale = 1.8f;
}

Turret::~Turret() {
    
}

void Turret::ActorInput(const Uint8 *keyState) {
    if(keyState[mLeftKey]) {
        mComp -> SetAngularSpeed(Math::TwoPi);
    }
    else if(keyState[mRightKey]) {
        mComp -> SetAngularSpeed(-(Math::TwoPi));
    }
    else {
        mComp -> SetAngularSpeed(0.0f);
    }
}

void Turret::SetPlayerTwo() {
    mLeftKey = SDL_SCANCODE_I;
    mRightKey = SDL_SCANCODE_P;
    mMesh -> SetTextureIndex(1);
}
