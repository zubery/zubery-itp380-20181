//
//  Plane.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Plane_hpp
#define Plane_hpp

#include <stdio.h>
#include "Actor.h"

class Plane : public Actor {
public:
    Plane(class Game* mGame);
    ~Plane();
    
private:
    
};

#endif /* Plane_hpp */
