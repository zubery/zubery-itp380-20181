//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include "Actor.h"
#include <SDL/SDL_image.h>
#include <algorithm>
#include "Component.h"
#include "SpriteComponent.h"
#include "Ship.hpp"

// TODO

Game::Game() {
  window = nullptr;
  renderer = nullptr;
  runner = true;
}

bool Game::Initialize() {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}

  window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, SDL_WINDOW_OPENGL);
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    
  int flags = IMG_INIT_PNG;
  int initted=IMG_Init(flags);
  if((initted & flags) != flags) {
    printf("IMG_Init: Failed to init required jpg and png support!\n");
    printf("IMG_Init: %s\n", IMG_GetError());
  }

    
    
  top.x = 0;
  top.y = 0;
  top.w = 1024;
  top.h = 20;
  right.x = 1024 - 20;
  right.y = 20;
  right.w = 20;
  right.h = 768 - 2*(20);
  bottom.x = 0;
  bottom.y = 768 - 20;
  bottom.w = 1024;
  bottom.h = 20;

  lastTicks = SDL_GetTicks();

  LoadData();
  return true;
}

void Game::RunLoop() {
  while (runner) {
    ProcessInput();
    UpdateGame();
    GenerateOutput();
  }
}

void Game::ProcessInput() {
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      runner = false;
    }
  }
  const Uint8 *state = SDL_GetKeyboardState(nullptr);
  for (auto i : actors) {
    i -> ProcessInput(state);
  }
    
  if (state[SDL_SCANCODE_ESCAPE]) {
    runner = false;
  }
}

void Game::UpdateGame() {
  while (SDL_GetTicks() - lastTicks < 16) {

  }

  deltaTime = SDL_GetTicks() - lastTicks;
  if (deltaTime > 50) {
    deltaTime = 50;
  }
  deltaTime = deltaTime / 1000.0f;
    
  std::vector<Actor*> actorClones = actors;
  for (auto i : actorClones) {
    i -> Update(deltaTime);
  }
  std::vector<Actor*> deadActors;
  for (auto i : actors) {
      //2 is the state EDead
      if (i -> GetState() == 2) {
          deadActors.push_back(i);
      }
  }
  for (auto i : deadActors) {
    delete i;
  }
}

void Game::GenerateOutput() {
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
  SDL_RenderClear(renderer);

  //SDL_RenderCopy(renderer, GetTexture("Assets/Stars.png"), nullptr, nullptr);
    for(auto it = sprites.begin(); it != sprites.end(); it++) {
        (*it) -> Draw(renderer);
    }
    
  SDL_RenderPresent(renderer);
}

void Game::Shutdown() {
  UnloadData();
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  IMG_Quit();
}

void Game::AddActor(class Actor* newActor) {
    actors.push_back(newActor);
}

void Game::RemoveActor(class Actor* remActor) {
    auto it = std::find(actors.begin(), actors.end(), remActor);
    actors.erase(it);
}

void Game::LoadData() {
    LoadTexture("Assets/Stars.png");
    LoadTexture("Assets/Asteroid.png");
    LoadTexture("Assets/Laser.png");
    LoadTexture("Assets/Ship.png");
    LoadTexture("Assets/ShipThrust.png");

    Actor* stars = new Actor(this);
    stars -> SetPosition(Vector2(512, 384));
    SpriteComponent* scStars = new SpriteComponent(stars, 75);
    scStars -> SetTexture(GetTexture("Assets/Stars.png"));
    stars->SetSprite(scStars);
    
    Ship* myShip = new Ship(this);
    myShip -> SetPosition(Vector2(512, 384));
}

void Game::UnloadData() {
  while (actors.size() != 0) {
    delete actors.back();
  }
    for(auto it = textureMap.begin(); it != textureMap.end(); it++) {
        SDL_DestroyTexture(it -> second);
    }
    textureMap.clear();
}

void Game::LoadTexture(std::string filename) {
    image = IMG_Load(filename.c_str());
    texture = SDL_CreateTextureFromSurface(renderer, image);
    textureMap.insert(std::pair<std::string, SDL_Texture*>(filename, texture));
    SDL_FreeSurface(image);
}

SDL_Texture* Game::GetTexture(std::string filename) {
    std::unordered_map<std::string, SDL_Texture*>::const_iterator it;
    it = textureMap.find(filename);
    if(it == textureMap.end()) {
        return nullptr;
    }
    else {
        return it -> second;
    }
}

void Game::AddSprite(class SpriteComponent *newSprite) {
    sprites.push_back(newSprite);
    std::sort(sprites.begin(), sprites.end(),
              [](SpriteComponent* a, SpriteComponent* b) {
                  return a->GetDrawOrder() < b->GetDrawOrder();
              });
}

void Game::RemoveSprite(class SpriteComponent *remSprite) {
    auto it = std::find(sprites.begin(), sprites.end(), remSprite);
    sprites.erase(it);
}


