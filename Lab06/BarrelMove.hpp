//
//  BarrelMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef BarrelMove_hpp
#define BarrelMove_hpp

#include <stdio.h>
#include "MoveComponent.h"

class BarrelMove : public MoveComponent {
public:
    BarrelMove(class Actor* mOwner);
    ~BarrelMove();
    void Update(float deltaTime) override;
    
private:
    float mYSpeed;
    float initialPX;
    float initialPY;
};

#endif /* BarrelMove_hpp */
