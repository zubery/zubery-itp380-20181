#pragma once
#include "SDL/SDL.h"

// TODO

class Game {
public:
  Game();
  bool Initialize();
  void Shutdown();
  void RunLoop();

private:
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Event event;

  SDL_Rect top;
  SDL_Rect right;
  SDL_Rect bottom;
  SDL_Rect paddle;
  SDL_Point paddleCenter;
  SDL_Rect ball;
  SDL_Point ballCenter;
  SDL_Point velocity;

  bool runner;
  float lastTicks;
  float deltaTime;
  int movement;
  int speed;

  void ProcessInput();
  void UpdateGame();
  void GenerateOutput();
};
