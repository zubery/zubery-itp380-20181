//
//  Turret.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Turret_hpp
#define Turret_hpp

#include <stdio.h>
#include "Actor.h"
#include "Game.h"

class Turret : public Actor {
public:
    Turret(class Game* mGame);
    ~Turret();
    void ActorInput(const Uint8* keyState);
    void SetPlayerTwo();
    
private:
    Uint8 mLeftKey = SDL_SCANCODE_Q;
    Uint8 mRightKey = SDL_SCANCODE_E;
    bool pressed = false;
};

#endif /* Turret_hpp */
