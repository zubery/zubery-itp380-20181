#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <unordered_map>
#include <string>
#include <vector>
#include <queue>
#include "Math.h"

class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

	void LoadSound(const std::string& fileName);
	Mix_Chunk* GetSound(const std::string& fileName);

	void LoadLevel(const std::string& fileName);

	class Renderer* GetRenderer() {	return mRenderer; }
    void AddBlock(class Block* block);
    void RemoveBlock(class Block* block);
    std::vector<class Block*>& GetBlocks() { return mBlocks; }
    int GetNumBlocks() { return (int)mBlocks.size(); }
    
    void AddCheckpoint(class Checkpoint* check) { mCheckpoints.push(check); }
    void RemoveCheckpoint() { mCheckpoints.pop(); }
    bool EmptyCheckpointQueue() { return mCheckpoints.empty(); }
    class Checkpoint* NextCheckpoint() { return mCheckpoints.front(); }

    class Player* GetPlayer() { return mPlayer; }
    void SetPlayer(class Player* player) { mPlayer = player; }
    
    class Arrow* GetArrow() { return mArrow; }
    void SetArrow(class Arrow* arrow) { mArrow = arrow; }
    
    std::string GetNextLevel() { return mNextLevel; }
    void SetNextLevel(std::string nextLevel) { mNextLevel = nextLevel; }
    void LoadNextLevel();
    
private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();
	void LoadData();
	void UnloadData();

	// Map of textures loaded
	std::unordered_map<std::string, SDL_Texture*> mTextures;
	std::unordered_map<std::string, Mix_Chunk*> mSounds;

	// All the actors in the game
	std::vector<class Actor*> mActors;
    std::vector<class Block*> mBlocks;
    std::queue<class Checkpoint*> mCheckpoints;

	class Renderer* mRenderer;

	Uint32 mTicksCount;
	bool mIsRunning;
    class Player* mPlayer;
    class Arrow* mArrow;
    std::string mNextLevel;
};
