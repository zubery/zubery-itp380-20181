//
//  BallMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/3/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "BallMove.hpp"
#include "Actor.h"
#include "CollisionComponent.h"
#include "Game.h"
#include "Paddle.hpp"
#include "Math.h"
#include "Block.hpp"

BallMove::BallMove(Actor* mOwner) : MoveComponent(mOwner){
    velocity = Vector2(100, -100);
}

BallMove::~BallMove() {
    
}

void BallMove::Update(float deltaTime) {
    mOwner -> SetPosition(Vector2((mOwner -> GetPosition()).x + (velocity.x * deltaTime), (mOwner -> GetPosition()).y + (velocity.y * deltaTime)));
    
    if((mOwner -> GetPosition()).x >= 1024 - (32 + 11)) {
        mOwner -> SetPosition(Vector2(1024 - (32 + 11), (mOwner -> GetPosition()).y));
        velocity.x *= -1;
    }
    else if((mOwner -> GetPosition()).x <= (32 + 11)) {
        mOwner -> SetPosition(Vector2((32 + 11), (mOwner -> GetPosition()).y));
        velocity.x *= -1;
    }
    else if((mOwner -> GetPosition()).y <= (32 + 11)) {
        mOwner -> SetPosition(Vector2((mOwner -> GetPosition()).x, 32 + 11));
        velocity.y *= -1;
    }
    //100 is just a number here to make the ball not seemingly clip off screen
    else if((mOwner -> GetPosition()).y >= (712 + 11 + 100)) {
        mOwner -> SetPosition(Vector2(512, 650));
        velocity.x = 100;
        velocity.y = -100;
    }
    
    bool collided = false;
    for (int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = mOwner -> GetGame() -> GetBlock(i);
        
        if((mOwner -> GetCollisionComponent()) -> Intersect(currBlock -> GetCollisionComponent())) {
            Vector2 angle;
            angle.x = (mOwner -> GetPosition()).x - (currBlock -> GetPosition()).x;
            angle.y = (mOwner -> GetPosition()).y - (currBlock -> GetPosition()).y;
            
            float angleNum = atan2(angle.y, angle.x);
            
            if(angleNum >= (2*M_PI) - (M_PI / 4.0f) || angleNum <= (M_PI / 4.0f)) {
                velocity.x *= -1;
            }
            else if(angleNum <= 3*(M_PI / 4.0f)) {
                velocity.y *= -1;
            }
            else if(angleNum <= 5*(M_PI / 4.0f)) {
                velocity.x *= -1;
            }
            else {
                velocity.y *= -1;
            }
            
            //EDead = 2
            currBlock -> SetState(Actor::EDead);
            collided = true;
        }
        
        if(collided) {
            i = mOwner -> GetGame() -> GetNumBlocks();
        }
    }
    
    if((mOwner -> GetCollisionComponent()) -> Intersect(mOwner -> GetGame() -> GetPaddle() -> GetCollisionComponent())) {
        
        if (mOwner -> GetCollisionComponent() -> GetMax().x < mOwner -> GetGame() -> GetPaddle() -> GetCollisionComponent() -> GetMax().x && mOwner -> GetCollisionComponent() -> GetMin().x > mOwner -> GetGame() -> GetPaddle() -> GetCollisionComponent() -> GetMin().x) {
             mOwner -> SetPosition(Vector2((mOwner -> GetPosition()).x, (mOwner -> GetGame() -> GetPaddle() -> GetCollisionComponent() -> GetMin().y) - 5));
        }
        
        
        if((mOwner -> GetCollisionComponent() -> GetMax()).x < (mOwner -> GetGame() -> GetPaddle() -> GetPosition()).x - ((mOwner -> GetGame() -> GetPaddle() -> GetCollisionComponent() -> GetWidth()) / 6.0f )) {
            if(velocity.y > 0) {
                velocity = Vector2::Reflect(velocity, Vector2(-0.707f, -0.707f));
            }
        }
        else if((mOwner -> GetCollisionComponent() -> GetMin()).x > (mOwner -> GetGame() -> GetPaddle() -> GetPosition()).x + ((mOwner -> GetGame() -> GetPaddle() -> GetCollisionComponent() -> GetWidth()) / 6.0f )) {
            if(velocity.y > 0) {
                velocity = Vector2::Reflect(velocity, Vector2(0.707f, -0.707f));
            }
        }
        else {
            if(velocity.y > 0) {
                velocity = Vector2::Reflect(velocity, Vector2(0.0f, -1.0f));
            }
        }
    }
}
