//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"

// TODO

Game::Game() {
  window = nullptr;
  renderer = nullptr;
  runner = true;
}

bool Game::Initialize() {
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}

  window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, SDL_WINDOW_OPENGL);
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  top.x = 0;
  top.y = 0;
  top.w = 1024;
  top.h = 20;
  right.x = 1024 - 20;
  right.y = 20;
  right.w = 20;
  right.h = 768 - 2*(20);
  bottom.x = 0;
  bottom.y = 768 - 20;
  bottom.w = 1024;
  bottom.h = 20;

  paddle.w = 10;
  paddle.h = 50;
  paddleCenter.x = 15;
  paddleCenter.y = 768 / 2;

  ball.w = 6;
  ball.h = 6;
  ballCenter.x = 1024 / 2;
  ballCenter.y = 768 / 2;
  velocity.x = -80;
  velocity.y = 80;

  lastTicks = SDL_GetTicks();

  return true;
}

void Game::RunLoop() {
  while (runner) {
    ProcessInput();
    UpdateGame();
    GenerateOutput();
  }
}

void Game::ProcessInput() {
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      runner = false;
    }
  }
  const Uint8 *state = SDL_GetKeyboardState(nullptr);
  if (state[SDL_SCANCODE_ESCAPE]) {
    runner = false;
  }

  if (state[SDL_SCANCODE_UP]) {
    movement = 0;
  }
  else if (state[SDL_SCANCODE_DOWN]) {
    movement = 1;
  }
  else {
    movement = -1;
  }
}

void Game::UpdateGame() {
  while (SDL_GetTicks() - lastTicks < 16) {

  }

  deltaTime = SDL_GetTicks() - lastTicks;
  if (deltaTime > 50) {
    deltaTime = 50;
  }
  deltaTime = deltaTime / 1000.0f;

  if (movement == 0) {
    speed = -90;
  } 
  else if (movement == 1) {
    speed = 90;
  }
  else if (movement == -1) {
    speed = 0;
  }

  paddleCenter.y = paddleCenter.y + (speed * deltaTime);

  if (paddleCenter.y < 20 + 25) {
    paddleCenter.y = 20 + 25;
  }
  if (paddleCenter.y > 768 - 20 - 25) {
    paddleCenter.y = 768 - 20 - 25;
  }

  ballCenter.x = ballCenter.x + (velocity.x * deltaTime);
  ballCenter.y = ballCenter.y + (velocity.y * deltaTime);

  //Right wall collision
  if (ballCenter.x + 3 >= right.x) {
    ballCenter.x = right.x - 3;
    velocity.x *= -1;
  }
  //Paddle collision
  else if (ballCenter.x - 3 <= paddleCenter.x + 5) {
    if (ballCenter.y + 3 < paddleCenter.y + 25 && ballCenter.y - 3 > paddleCenter.y - 25) {
      if (!(ballCenter.x - 3 <= paddleCenter.x - 25)) {
        ballCenter.x = paddleCenter.x + 5 + 3;
        velocity.x *= -1;
      }
    }
  }
  //Top wall collision
  else if (ballCenter.y - 3 <= top.y + top.h) {
    ballCenter.y = top.y + top.h + 3;
    velocity.y *= -1;
  }
  //Bottom wall collision
  else if (ballCenter.y + 3 >= bottom.y) {
    ballCenter.y = bottom.y - 3;
    velocity.y *= -1;
  }

  if (ballCenter.x + 3 <= 0) {
    runner = false;
  }
}

void Game::GenerateOutput() {
  SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
  SDL_RenderClear(renderer);

  paddle.x = paddleCenter.x - 5;
  paddle.y = paddleCenter.y - 25;

  ball.x = ballCenter.x - 3;
  ball.y = ballCenter.y - 3;

  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

  SDL_RenderDrawRect(renderer, &top);
  SDL_RenderDrawRect(renderer, &right);
  SDL_RenderDrawRect(renderer, &bottom);
  SDL_RenderDrawRect(renderer, &paddle);
  SDL_RenderDrawRect(renderer, &ball);

  SDL_RenderFillRect(renderer, &top);
  SDL_RenderFillRect(renderer, &right);
  SDL_RenderFillRect(renderer, &bottom);
  SDL_RenderFillRect(renderer, &paddle);
  SDL_RenderFillRect(renderer, &ball);

  SDL_RenderPresent(renderer);
}

void Game::Shutdown() {
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
