#pragma once
#include "SDL/SDL.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <vector>
#include "Math.h"

// TODO

class Game {
public:
  Game();
  bool Initialize();
  void Shutdown();
  void RunLoop();
  void AddActor(class Actor* newActor);
  void RemoveActor(class Actor* remActor);
  void LoadTexture(std::string filename);
  SDL_Texture* GetTexture(std::string filename);
    void AddSprite(class SpriteComponent* newSprite);
    void RemoveSprite(class SpriteComponent* remSprite);
    void AddBlock(class Block* mBlock);
    void RemBlock(class Block* mBlock);
    Block* GetBlock(int it);
    int GetNumBlocks() { return blocks.size(); }
    class Player* GetPlayer() { return mPlayer; }
    void LoadSound(const std::string& filename);
    class Mix_Chunk* GetSound(const std::string& filename);
    const Vector2& GetCameraPos() const { return mCameraPos; }
    void SetCameraPos(const Vector2& cameraPos) { mCameraPos = cameraPos; }
        void LoadNextLevel();
    int getLevelXPos() { return levelxpos; }

private:
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Surface *image;
  SDL_Texture *texture;
  SDL_Event event;

  SDL_Rect top;
  SDL_Rect right;
  SDL_Rect bottom;
    
    int levelNum = 0;
    bool firstLoad = true;
    int levelxpos = 0;
    
    
  std::unordered_map<std::string, SDL_Texture*> textureMap;
    std::unordered_map<std::string, class Mix_Chunk*> soundMap;
    
  std::vector<class Actor*> actors;
    std::vector<class SpriteComponent*> sprites;

  bool runner;
  float lastTicks;
  float deltaTime;
    
    class Player* mPlayer;

  void ProcessInput();
  void UpdateGame();
  void GenerateOutput();
    void LoadData();
  void UnloadData();
    
    std::vector<class Block*> blocks;
    Vector2 mCameraPos;
};
