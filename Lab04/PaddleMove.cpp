//
//  PaddleMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/3/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "PaddleMove.hpp"
#include "Paddle.hpp"

PaddleMove::PaddleMove(Paddle* owner) : MoveComponent(owner){
    
}

PaddleMove::~PaddleMove() {
    
}

void PaddleMove::Update(float deltaTime) {
    this -> MoveComponent::Update(deltaTime);
    
    if((mOwner -> GetPosition()).x <= 32 + 52) {
        mOwner -> SetPosition(Vector2(32 + 52, 730));
    }
    else if((mOwner -> GetPosition()).x >= 1024 - (32 + 52)) {
        mOwner -> SetPosition(Vector2(1024 - (32 + 52), 730));
    }
}
