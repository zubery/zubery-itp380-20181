#pragma once
#include "SDL/SDL.h"
#include <vector>
#include <unordered_map>
#include <string>

// TODO

class Game {
public:
  Game();
  bool Initialize();
  void Shutdown();
  void RunLoop();
  void AddActor(class Actor* newActor);
  void RemoveActor(class Actor* remActor);
  void LoadTexture(std::string filename);
  SDL_Texture* GetTexture(std::string filename);
    void AddSprite(class SpriteComponent* newSprite);
    void RemoveSprite(class SpriteComponent* remSprite);

private:
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Surface *image;
  SDL_Texture *texture;
  SDL_Event event;

  SDL_Rect top;
  SDL_Rect right;
  SDL_Rect bottom;
    
  std::unordered_map<std::string, SDL_Texture*> textureMap;
    
  std::vector<class Actor*> actors;
    std::vector<class SpriteComponent*> sprites;

  bool runner;
  float lastTicks;
  float deltaTime;

  void ProcessInput();
  void UpdateGame();
  void GenerateOutput();
  void LoadData();
  void UnloadData();
};
