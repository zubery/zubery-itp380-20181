//
//  TankMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "TankMove.hpp"
#include "Actor.h"
#include "CollisionComponent.h"
#include "Block.hpp"

TankMove::TankMove(Actor* mOwner) : MoveComponent(mOwner) {
    
}

TankMove::~TankMove() {
    
}

void TankMove::Update(float deltaTime) {
    MoveComponent::Update(deltaTime);
    
    float newX = mOwner -> GetPosition().x;
    float newY = mOwner -> GetPosition().y;
    float newZ = mOwner -> GetPosition().z;
    
    mOwner -> SetPosition(Vector3(newX, newY, newZ));
    
    float playerYMin = (mOwner -> GetCollisionComponent() -> GetMin().y);
    float playerYMax = (mOwner -> GetCollisionComponent() -> GetMax().y);
    float playerXMin = (mOwner -> GetCollisionComponent() -> GetMin().x);
    float playerXMax = (mOwner -> GetCollisionComponent() -> GetMax().x);
    
    for (int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = (mOwner -> GetGame() -> GetBlocks())[i];
        if ((mOwner -> GetCollisionComponent()) -> Intersect(currBlock -> GetCollisionComponent())) {
            float dx1, dx2, dy1, dy2;
            float tempx1, tempx2, tempy1, tempy2;
            dx2 = (currBlock -> GetCollisionComponent() -> GetMax().x) - playerXMin;
            dx1 = (currBlock -> GetCollisionComponent() -> GetMin().x) - playerXMax;
            dy1 = (currBlock -> GetCollisionComponent() -> GetMin().y) - playerYMax;
            dy2 = (currBlock -> GetCollisionComponent() -> GetMax().y) - playerYMin;
            
            tempx2 = dx2;
            tempx1 = dx1;
            tempy2 = dy2;
            tempy1 = dy1;
            
            if(dx2 < 0) {
                tempx2 = dx2 * -1;
            }
            if(dx1 < 0) {
                tempx1 = dx1 * -1;
            }
            if(dy2 < 0) {
                tempy2 = dy2 * -1;
            }
            if(dy1 < 0) {
                tempy1 = dy1 * -1;
            }
            
            float smallest = std::min(tempx1, std::min(tempx2, std::min(tempy1, tempy2)));
            
            if(smallest == tempy1) {
                newY += dy1;
                playerYMin += dy1;
                playerYMax += dy1;
            }
            else if(smallest == tempy2) {
                newY += dy2;
                playerYMin += dy2;
                playerYMax += dy2;
            }
            else if(smallest == tempx1) {
                newX += dx1;
                playerXMin += dx1;
                playerXMax += dx1;
            }
            else {
                newX += dx2;
                playerXMin += dx2;
                playerXMax += dx2;
            }
        }
    }
    
    mOwner -> SetPosition(Vector3(newX, newY, newZ));
}

void TankMove::ProcessInput(const Uint8 *keyState) {
    if(keyState[mForwardKey]) {
        this -> SetForwardSpeed(250.0f);
    }
    else if(keyState[mLeftKey]) {
        this -> SetAngularSpeed(Math::TwoPi);
    }
    else if(keyState[mBackKey]) {
        this -> SetForwardSpeed(-250.0f);
    }
    else if(keyState[mRightKey]) {
        this -> SetAngularSpeed(-(Math::TwoPi));
    }
    else {
        this -> SetForwardSpeed(0.0f);
        this -> SetAngularSpeed(0.0f);
    }
}

void TankMove::SetPlayerTwo() {
    mForwardKey = SDL_SCANCODE_O;
    mLeftKey = SDL_SCANCODE_K;
    mBackKey = SDL_SCANCODE_L;
    mRightKey = SDL_SCANCODE_SEMICOLON;
}
