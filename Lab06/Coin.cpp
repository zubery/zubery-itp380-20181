//
//  Coin.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/27/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Coin.hpp"
#include "Game.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "Player.hpp"
#include "AnimatedSprite.hpp"
#include <SDL/SDL_mixer.h>

Coin::Coin(Game* mGame) : Actor(mGame) {
    SpriteComponent* coinSprite = new AnimatedSprite(this);
    
    AnimatedSprite* tempSprite = static_cast<AnimatedSprite*>(coinSprite);
    for (int i = 0; i < 16; i++) {
        tempSprite -> AddImage(mGame -> GetTexture("Assets/Coin/coin" + std::to_string(i + 1) + ".png"));
    }
    
    mSprite = coinSprite;
    mColl = new CollisionComponent(this);
    mColl -> SetSize(32.0f, 32.0f);
}

Coin::~Coin() {
    
}

void Coin::UpdateActor(float deltaTime) {
    if(mColl -> Intersect(mGame -> GetPlayer() -> GetCollisionComponent())) {
        Mix_Chunk *chunk = mGame -> GetSound("Assets/Coin/coin.wav");
        Mix_PlayChannel(-1, chunk, 0);
        mState = EDead;
    }
    if(this -> GetPosition().x < this -> GetGame() -> GetCameraPos().x) {
        mState = EDead;
    }
}
