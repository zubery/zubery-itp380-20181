//
//  Barrel.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Barrel_hpp
#define Barrel_hpp

#include <stdio.h>
#include "Actor.h"

class Barrel : public Actor {
public:
    Barrel(class Game* mGame);
    ~Barrel();
};

#endif /* Barrel_hpp */
