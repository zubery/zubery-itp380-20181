//
//  Tank.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Tank_hpp
#define Tank_hpp

#include <stdio.h>
#include "Actor.h"
#include "Game.h"

class Tank : public Actor {
public:
    Tank(class Game* mGame);
    ~Tank();
    void UpdateActor(float deltaTime) override;
    void SetPlayerTwo();
    void Fire();
    void Respawn();
    void SetInitialPos(Vector3 initial) { initialPos = initial; }
    void ActorInput(const Uint8* keyState) override;
private:
    class Turret* mTurret;
    Vector3 initialPos;
    Uint8 fireKey = SDL_SCANCODE_LSHIFT;
    bool pressed;
};

#endif /* Tank_hpp */
