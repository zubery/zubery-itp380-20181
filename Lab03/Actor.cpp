#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include <algorithm>
#include "SpriteComponent.h"
#include "MoveComponent.h"

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
{
    mGame -> AddActor(this);
    mSprite = nullptr;
    mComp = nullptr;
}

Actor::~Actor()
{
  mGame -> RemoveActor(this);
    if (mSprite != nullptr) {
        delete mSprite;
    }
    if (mComp != nullptr) {
        delete mComp;
    }
}

void Actor::Update(float deltaTime)
{
    if (mState == EActive) {
        //Update comps
        if(mComp != nullptr) {
            mComp -> Update(deltaTime);
        }
        if(mSprite != nullptr) {
            mSprite -> Update(deltaTime);
        }
        UpdateActor(deltaTime);
    }
}

void Actor::UpdateActor(float deltaTime)
{
}

void Actor::ProcessInput(const Uint8* keyState)
{
    if (mState == EActive) {
        //Call process input on comps
        //Call actor input on comps
        if(mComp != nullptr) {
            mComp -> ProcessInput(keyState);
        }
        mSprite -> ProcessInput(keyState);
        ActorInput(keyState);
    }
}

void Actor::ActorInput(const Uint8* keyState)
{
}

Vector2 Actor::GetForward() {
    double y = sin(mRotation) * -1;
    double x = cos(mRotation);
    
    return Vector2(x, y);
}
