#include "MoveComponent.h"
#include "Actor.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
    mOwner = owner;
}

void MoveComponent::Update(float deltaTime)
{
	// TODO: Implement in Part 3
    mOwner -> SetRotation((mOwner -> GetRotation()) + (mAngularSpeed * deltaTime));
    mOwner -> SetPosition(Vector2(((mOwner -> GetForward()).x * mForwardSpeed) + (mOwner -> GetPosition()).x, ((mOwner -> GetForward()).y * mForwardSpeed) + (mOwner -> GetPosition()).y));
}
