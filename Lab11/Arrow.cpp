//
//  Arrow.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 4/18/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Arrow.hpp"
#include "Game.h"
#include "MeshComponent.h"
#include "Renderer.h"
#include "Player.hpp"
#include "Checkpoint.hpp"
#include <iostream>

Arrow::Arrow(Game* game) : Actor(game) {
    mMesh = new MeshComponent(this);
    mMesh -> SetMesh(mGame->GetRenderer()->GetMesh("Assets/Arrow.gpmesh"));
    this -> SetScale(0.15f);
}

Arrow::~Arrow() {
    
}

void Arrow::UpdateActor(float deltaTime) {
    
    if(mGame -> EmptyCheckpointQueue()) {
        this -> SetQuat(Quaternion::Identity);
    }
    else if(mGame -> NextCheckpoint() == nullptr) {
        this -> SetQuat(Quaternion::Identity);
    }
    else {
        Vector3 activeCheckpoint = mGame -> NextCheckpoint() -> GetPosition();
        Vector3 playerpos = mGame -> GetPlayer() -> GetPosition();
        Vector3 next = activeCheckpoint - playerpos;
        Vector3 normalnext = Vector3::Normalize(next);
        
        float angle = acos(Vector3::Dot(Vector3(1.0f, 0.0f, 0.0f), normalnext));
        Vector3 axis = Vector3::Cross(Vector3(1.0f, 0.0f, 0.0f), normalnext);
        
        if(Math::NearZero(axis.Length())) {
            this -> SetQuat(Quaternion::Identity);
        }
        else {
            Vector3 axisnormal = Vector3::Normalize(axis);
            this -> SetQuat(Quaternion(axisnormal, angle));
        }
    }
    
    this -> SetPosition(mGame -> GetRenderer() -> Unproject(Vector3(0.0f, 250.0f, 0.1f)));
    //std::cout << "HELLO FROM THE OTHER SIDE" << std::endl;
}
