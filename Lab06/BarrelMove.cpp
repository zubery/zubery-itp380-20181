//
//  BarrelMove.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "BarrelMove.hpp"
#include "Actor.h"
#include "Game.h"
#include "Block.hpp"
#include "CollisionComponent.h"
#include "Player.hpp"

BarrelMove::BarrelMove(Actor* mOwner) : MoveComponent(mOwner) {
    this -> SetForwardSpeed(80.0f);
    this -> SetAngularSpeed(-2 * M_PI);
}

BarrelMove::~BarrelMove() {
    
}

void BarrelMove::Update(float deltaTime) {
    mOwner -> SetRotation(this -> GetAngularSpeed() * deltaTime);
    
    float newX = mOwner -> GetPosition().x + (this -> GetForwardSpeed() * deltaTime);
    float newY = mOwner -> GetPosition().y + (mYSpeed * deltaTime);
    
    if(newY >= 768.0f) {
        mOwner -> SetState(Actor::EDead);
    }
    
    for (int i = 0; i < mOwner -> GetGame() -> GetNumBlocks(); i++) {
        Block* currBlock = mOwner -> GetGame() -> GetBlock(i);
        if ((mOwner -> GetCollisionComponent()) -> Intersect(currBlock -> GetCollisionComponent())) {
            float dy1 = (currBlock -> GetCollisionComponent() -> GetMin().y) - (mOwner -> GetCollisionComponent() -> GetMax().y);
            
            newY += dy1;
            
            mYSpeed = 0.0f;
        }
    }
    
    if (mOwner -> GetGame() -> GetPlayer() -> GetCollisionComponent() -> Intersect(mOwner -> GetCollisionComponent())) {
        float initx = mOwner -> GetGame() -> GetPlayer() -> GetX();
        float inity = mOwner -> GetGame() -> GetPlayer() -> GetY();
        
        mOwner -> GetGame() -> GetPlayer() -> SetPosition(Vector2(initx, inity));
    }
    
    mOwner -> SetPosition(Vector2(newX, newY));
    
    mYSpeed += (800.0f * deltaTime);
}
