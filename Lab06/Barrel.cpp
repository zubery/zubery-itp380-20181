//
//  Barrel.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Barrel.hpp"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "Game.h"
#include "BarrelMove.hpp"

Barrel::Barrel(Game* mGame) : Actor(mGame) {
    SpriteComponent* newSprite = new SpriteComponent(this, 150);
    newSprite -> SetTexture(mGame -> GetTexture("Assets/Barrel.png"));
    
    mSprite = newSprite;
    mColl = new CollisionComponent(this);
    mColl -> SetSize(32.0f, 32.0f);
    mComp = new BarrelMove(this);
}

Barrel::~Barrel() {
    
}
