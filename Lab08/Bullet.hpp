//
//  Bullet.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Bullet_hpp
#define Bullet_hpp

#include <stdio.h>
#include "Actor.h"

class Bullet : public Actor {
public:
    Bullet(class Game* mGame);
    ~Bullet();
    void SetShot(class Tank* shooter) { whoShot = shooter; }
    class Tank* GetShot() { return whoShot; }
    void UpdateActor(float deltaTime) override;
private:
    class Tank* whoShot;
};

#endif /* Bullet_hpp */
