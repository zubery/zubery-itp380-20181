//
//  MoveComponent.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "MoveComponent.h"
#include "Actor.h"

MoveComponent::MoveComponent(class Actor* owner)
:Component(owner)
,mAngularSpeed(0.0f)
,mForwardSpeed(0.0f)
{
    mOwner = owner;
    mStrafeSpeed = 0.0f;
}

void MoveComponent::Update(float deltaTime)
{
    // TODO: Implement in Part 3
    mOwner -> SetRotation((mOwner -> GetRotation()) + (mAngularSpeed * deltaTime));
    mOwner -> SetPosition(Vector3(((mOwner -> GetForward()).x * mForwardSpeed * deltaTime) + (mOwner -> GetPosition()).x, ((mOwner -> GetForward()).y * mForwardSpeed * deltaTime) + (mOwner -> GetPosition()).y, ((mOwner -> GetForward()).z * mForwardSpeed * deltaTime) + (mOwner -> GetPosition()).z));
    mOwner -> SetPosition(mOwner -> GetPosition() + ((mOwner -> GetRight()) * (mOwner -> GetMoveComponent() -> GetStrafeSpeed() * deltaTime)));
}
