//
//  PaddleMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/3/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PaddleMove_hpp
#define PaddleMove_hpp

#include <stdio.h>
#include "MoveComponent.h"

class PaddleMove : public MoveComponent {
public:
    PaddleMove(class Paddle* owner);
    ~PaddleMove();
    
    void Update(float deltaTime) override;
};

#endif /* PaddleMove_hpp */
