//
//  PlayerMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef PlayerMove_hpp
#define PlayerMove_hpp

#include <stdio.h>
#include "MoveComponent.h"
#include "Game.h"

class PlayerMove : public MoveComponent {
public:
    PlayerMove(class Actor* mOwner);
    ~PlayerMove();
    void Update(float deltaTime) override;
    void ProcessInput(const Uint8* keyState) override;
    
private:
    float mYSpeed;
    bool mSpacePressed;
    bool mInAir;
};

#endif /* PlayerMove_hpp */
