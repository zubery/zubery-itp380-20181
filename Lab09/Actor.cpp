#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include <algorithm>
#include "MoveComponent.h"
#include "CollisionComponent.h"
#include "MeshComponent.h"
#include "CameraComponent.hpp"

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector3::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
{
    mGame -> AddActor(this);
    mComp = nullptr;
    mColl = nullptr;
    mMesh = nullptr;
    mCam = nullptr;
}

Actor::~Actor()
{
  mGame -> RemoveActor(this);
    if (mComp != nullptr) {
        delete mComp;
    }
    if (mColl != nullptr) {
        delete mColl;
    }
    if (mMesh != nullptr) {
        delete mMesh;
    }
    if (mCam != nullptr) {
        delete mCam;
    }
}

void Actor::Update(float deltaTime)
{
    if (mState == EActive) {
        //Update comps
        if(mComp != nullptr) {
            mComp -> Update(deltaTime);
        }
        if(mColl != nullptr) {
            mColl -> Update(deltaTime);
        }
        if(mMesh != nullptr) {
            mMesh -> Update(deltaTime);
        }
        if(mCam != nullptr) {
            mCam -> Update(deltaTime);
        }
        UpdateActor(deltaTime);
    }
    
    Matrix4 scale = Matrix4::CreateScale(mScale);
    Matrix4 rotationz = Matrix4::CreateRotationZ(mRotation);
    Matrix4 position = Matrix4::CreateTranslation(mPosition);
    mWorldTransform = scale * rotationz * position;
}

void Actor::UpdateActor(float deltaTime)
{
}

void Actor::ProcessInput(const Uint8* keyState)
{
    if (mState == EActive) {
        //Call process input on comps
        //Call actor input on comps
        if(mComp != nullptr) {
            mComp -> ProcessInput(keyState);
        }
        if (mColl != nullptr) {
            mColl -> ProcessInput(keyState);
        }
        if (mMesh != nullptr) {
            mMesh -> ProcessInput(keyState);
        }
        if (mCam != nullptr) {
            mCam -> ProcessInput(keyState);
        }
        ActorInput(keyState);
    }
}

void Actor::ActorInput(const Uint8* keyState)
{
}

Vector3 Actor::GetForward() const
{
    return Vector3(Math::Cos(mRotation), Math::Sin(mRotation), 0.0f);
}

Vector3 Actor::GetRight() const {
    return Vector3(Math::Cos(mRotation + M_PI_2), Math::Sin(mRotation + M_PI_2), 0.0f);
}
