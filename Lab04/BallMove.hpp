//
//  BallMove.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/3/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef BallMove_hpp
#define BallMove_hpp

#include <stdio.h>
#include "MoveComponent.h"
#include "Math.h"

class BallMove : public MoveComponent {
public:
    BallMove(class Actor* mOwner);
    ~BallMove();
    
    Vector2 GetVelocity() { return velocity; };
    
    void Update(float deltaTime) override;
    
private:
    Vector2 velocity;
    bool paddleCollided;
};

#endif /* BallMove_hpp */
