//
//  Background.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Background_hpp
#define Background_hpp

#include <stdio.h>
#include "SpriteComponent.h"
#include <SDL/SDL_image.h>
#include <vector>

class Background : public SpriteComponent {
public:
    Background(class Actor* owner, int drawOrder);
    ~Background();
    void AddImage(SDL_Texture* image) { mTextures.push_back(image); }
    void Draw(SDL_Renderer* renderer) override;
    float getParallax() { return mParallax; }
    void setParallax(float newParallax) { mParallax = newParallax; }
    
private:
    std::vector<SDL_Texture*> mTextures;
    float mParallax = 1.0f;
};

#endif /* Background_hpp */
