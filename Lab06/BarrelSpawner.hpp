//
//  BarrelSpawner.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/21/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef BarrelSpawner_hpp
#define BarrelSpawner_hpp

#include <stdio.h>
#include "Actor.h"

class BarrelSpawner : public Actor {
public:
    BarrelSpawner(class Game* mGame);
    ~BarrelSpawner();
    void UpdateActor(float deltaTime) override;
private:
    float timer;
};

#endif /* BarrelSpawner_hpp */
