//
//  Tower.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Tower_hpp
#define Tower_hpp

#include <stdio.h>
#include "Actor.h"

class Tower : public Actor {
public:
    Tower(class Game* mGame);
    ~Tower();
    void UpdateActor(float deltaTime) override;
    class Plane* ClosestPlane();
    
private:
    float attackTimer = 4.0f; 
};

#endif /* Tower_hpp */
