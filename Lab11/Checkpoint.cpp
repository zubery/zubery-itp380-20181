//
//  Checkpoint.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 4/12/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Checkpoint.hpp"
#include "MeshComponent.h"
#include "CollisionComponent.h"
#include "Renderer.h"
#include "Player.hpp"

Checkpoint::Checkpoint(Game* game) : Actor(game) {
    mMesh = new MeshComponent(this);
    mMesh->SetMesh(mGame->GetRenderer()->GetMesh("Assets/Checkpoint.gpmesh"));
    mColl = new CollisionComponent(this);
    mColl -> SetSize(25.0f, 25.0f, 25.0f);
    
    if(mGame -> EmptyCheckpointQueue()) {
        active = true;
        mMesh->SetTextureIndex(0);
    }
    else {
        active = false;
        mMesh->SetTextureIndex(1);
    }
    
    mGame -> AddCheckpoint(this);
}

Checkpoint::~Checkpoint() {
    
}

void Checkpoint::UpdateActor(float deltatime) {
    if(active) {
        if(mColl -> Intersect(mGame -> GetPlayer() -> GetCollisionComponent())) {
            if(mLevelString.length() != 0) {
                mGame -> SetNextLevel(mLevelString);
            }
            
            Mix_Chunk* mix = mGame -> GetSound("Assets/Sounds/Checkpoint.wav");
            Mix_PlayChannel(-1, mix, 0);
            
            active = false;
            mMesh -> SetTextureIndex(1);
            mGame -> RemoveCheckpoint();
            mGame -> GetPlayer() -> SetRespawnPos(this -> GetPosition());
            
            Checkpoint* next;
            if(mGame -> EmptyCheckpointQueue()) {
                next =nullptr;
            }
            else {
                next = mGame -> NextCheckpoint();
            }
            
            if(next != nullptr) {
                next -> MakeActive();
            }
            this -> SetState(State::EDead);
        }
    }
}

void Checkpoint::MakeActive() {
    active = true;
    mMesh -> SetTextureIndex(0);
}
