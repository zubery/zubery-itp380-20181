//
//  Player.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/8/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "Actor.h"

class Player : public Actor {
public:
    Player(class Game* mGame);
    ~Player();
    float GetX() { return initialX; }
    float GetY() { return initialY; }
    
private:
    float initialX;
    float initialY;
};

#endif /* Player_hpp */
