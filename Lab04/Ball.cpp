//
//  Ball.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/3/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Ball.hpp"
#include "Game.h"
#include "SpriteComponent.h"
#include "BallMove.hpp"
#include "CollisionComponent.h"

Ball::Ball(Game* mGame) : Actor(mGame) {
    mSprite = new SpriteComponent(this);
    mSprite -> SetTexture(mGame -> GetTexture("Assets/Ball.png"));
    mComp = new BallMove(this);
    mColl = new CollisionComponent(this);
    mColl -> SetSize(10, 10);
    this -> SetPosition(Vector2(512, 650));
}

Ball::~Ball() {
    
}
