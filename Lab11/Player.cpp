//
//  Player.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Player.hpp"
#include "PlayerMove.hpp"
#include "Game.h"
#include "CollisionComponent.h"
#include "CameraComponent.hpp"

Player::Player(Game* game) : Actor(game) {
    mComp = new PlayerMove(this);
    mColl = new CollisionComponent(this);
    mColl -> SetSize(50.0f, 175.0f, 50.0f);
    mCam = new CameraComponent(this);
}

Player::~Player() {
    
}
