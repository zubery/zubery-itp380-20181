//
//  Bullet.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 3/7/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Bullet.hpp"
#include "Game.h"
#include "SpriteComponent.h"
#include "CollisionComponent.h"
#include "MoveComponent.h"
#include "Plane.hpp"

Bullet::Bullet(Game* mGame) : Actor(mGame) {
    mSprite = new SpriteComponent(this, 200);
    mSprite -> SetTexture(mGame -> GetTexture("Assets/Bullet.png"));
    mColl = new CollisionComponent(this);
    mColl -> SetSize(10, 10);
    mComp = new MoveComponent(this);
    mComp -> SetForwardSpeed(10.0f);
}

Bullet::~Bullet() {
    
}

void Bullet::UpdateActor(float deltaTime) {
    lifespan -= deltaTime;
    if(lifespan <= 0.0f) {
        this -> SetState(Actor::EDead);
    }
    else {
        std::vector<Plane*> myPlanes = *(mGame -> getPlanes());
        for (int i = 0; i < myPlanes.size(); i++) {
            if(this -> GetCollisionComponent() -> Intersect(myPlanes[i] -> GetCollisionComponent())) {
                this -> SetState(Actor::EDead);
                myPlanes[i] -> SetState(Actor::EDead);
            }
        }
    }
}
