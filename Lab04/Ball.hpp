//
//  Ball.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/3/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef Ball_hpp
#define Ball_hpp

#include <stdio.h>
#include "Actor.h"

class Ball : public Actor {
public:
    Ball(class Game* mGame);
    ~Ball();
};

#endif /* Ball_hpp */
