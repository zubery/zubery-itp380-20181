//
//  AnimatedSprite.hpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/27/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#ifndef AnimatedSprite_hpp
#define AnimatedSprite_hpp

#include <stdio.h>
#include "SpriteComponent.h"
#include <SDL/SDL_image.h>
#include <vector>

class AnimatedSprite : public SpriteComponent {
public:
    AnimatedSprite(class Actor* owner, int drawOrder = 100);
    ~AnimatedSprite();
    void setAnimSpeed(float newSpeed) { mAnimSpeed = newSpeed; }
    void AddImage(SDL_Texture* image) { mImages.push_back(image); }
    void Update(float deltaTime) override;
    
private:
    std::vector<SDL_Texture*> mImages;
    float mAnimTimer = 0.0f;
    float mAnimSpeed = 15.0f;
};

#endif /* AnimatedSprite_hpp */
