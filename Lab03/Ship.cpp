//
//  Ship.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 1/31/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Ship.hpp"
#include "Game.h"

Ship::Ship(class Game* mGame)
:Actor(mGame)
{
    mSprite = new SpriteComponent(this);
    mSprite -> SetTexture(mGame -> GetTexture("Assets/Ship.png"));
    this -> SetSprite(mSprite);
    mComp = new MoveComponent(this);
}

Ship::~Ship() {
    
}

void Ship::ActorInput(const Uint8* keyState) {
    if(keyState[SDL_SCANCODE_UP]) {
        mSprite -> SetTexture(mGame -> GetTexture("Assets/ShipThrust.png"));
        mComp -> SetForwardSpeed(5);
    }
    else if(keyState[SDL_SCANCODE_RIGHT]) {
        mComp -> SetAngularSpeed(1);
    }
    else if(keyState[SDL_SCANCODE_LEFT]) {
        mComp -> SetAngularSpeed(-1);
    }
    else if(keyState[SDL_SCANCODE_DOWN]) {
        mSprite -> SetTexture(mGame -> GetTexture("Assets/ShipThrust.png"));
        mComp -> SetForwardSpeed(-5);
    }
    else {
        mSprite -> SetTexture(mGame -> GetTexture("Assets/Ship.png"));
        mComp -> SetForwardSpeed(0);
        mComp -> SetAngularSpeed(0);
    }
}

