//
//  Background.cpp
//  Game-mac
//
//  Created by Ryan Zubery on 2/22/18.
//  Copyright © 2018 Sanjay Madhav. All rights reserved.
//

#include "Background.hpp"
#include "Actor.h"
#include "Game.h"
#include <SDL/SDL_image.h>

Background::Background(Actor* owner, int drawOrder) : SpriteComponent(owner, drawOrder){
    
}

Background::~Background() {
    
}

void Background::Draw(SDL_Renderer *renderer) {
    float posx, posy;
    posx = 0;
    posy = 0;
    
    int i = 0;
    int screen = 1024;
    
    while (posx - (mOwner -> GetGame() -> GetCameraPos().x) < screen) {
        this -> SetTexture(mTextures[i]);
        if (mTextures[i])
        {
            SDL_Rect r;
            r.w = static_cast<int>(mTexWidth * mOwner->GetScale());
            r.h = static_cast<int>(mTexHeight * mOwner->GetScale());
            // Center the rectangle around the position of the owner
            r.x = static_cast<int>(posx - mParallax * (mOwner -> GetGame() -> GetCameraPos().x));
            r.y = static_cast<int>(posy - mParallax * (mOwner -> GetGame() -> GetCameraPos().y));
            
            // Draw (have to convert angle from radians to degrees, and clockwise to counter)
            SDL_RenderCopyEx(renderer,
                             mTextures[i],
                             nullptr,
                             &r,
                             0.0f,
                             nullptr,
                             SDL_FLIP_NONE);
        }
        posx += mTexWidth;
        i++;
        if (i > 2) {
            i = 0;
        }
    }
}
